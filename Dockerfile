FROM python:3.11.2 AS build
WORKDIR /app

# Do not buffer stdout etc.
ENV PYTHONUNBUFFERED=1

# avoid writing .pyc (byte-code files)
ENV PYTHONDONTWRITEBYTECODE=1

# install a handler for SIGSEGV, SIGFPE, SIGABRT, SIGBUS and SIGILL
ENV PYTHONFAULTHANDLER=1

# Configure and install poetry
ENV POETRY_VERSION=1.4.1
ENV POETRY_HOME="/opt/poetry"
ENV POETRY_VIRTUALENVS_IN_PROJECT=true
ENV POETRY_NO_INTERACTION=1
RUN curl -sSL https://install.python-poetry.org | python3
ENV PATH="$POETRY_HOME/bin:$PATH"

# Install other tools
RUN mkdir tools
WORKDIR /app/tools
RUN curl -Os https://uploader.codecov.io/latest/linux/codecov
RUN chmod +x codecov
ENV PATH="/app/tools:$PATH"
WORKDIR /app

COPY poetry.lock pyproject.toml README.md ./
RUN poetry install

COPY src src
COPY tests tests

COPY .pylintrc ./
COPY .flake8 ./
COPY .coveragerc ./

RUN poetry run pylint src
RUN poetry run flake8 src


FROM build AS test
WORKDIR /app

COPY .coveragerc ./
COPY --chmod=0755 entrypoint_test.sh ./
ENTRYPOINT [ "/app/entrypoint_test.sh" ]


