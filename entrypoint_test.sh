#!/bin/sh -x

# exit if any command fails
set -e

mkdir -p /tmp/output
poetry run coverage run -m pytest --junitxml=/tmp/output/report.xml
poetry run coverage xml
poetry run coverage report -m

codecov -t ${CODECOV_TOKEN}