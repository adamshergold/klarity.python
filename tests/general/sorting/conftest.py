from random import randrange

import pytest


def create_list_of_ints(n):
    """Create list of random integers of length n"""
    max_int = 4096
    return [randrange(-max_int, max_int) for _ in range(0, randrange(0, n))]


def create_inputs(m, n):
    """Return list of list of sorting ints"""
    inputs = [create_list_of_ints(n) for i in range(0, m)]
    inputs.append([])
    return inputs


@pytest.fixture(params=create_inputs(100, 64), ids=str)
def sorting_inputs(request):
    """Test fixture for sorting tests"""
    # We need to copy so that we don't end up using sorted lists for other cases
    return request.param.copy()
