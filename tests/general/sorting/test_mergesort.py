from general.sorting.mergesort import sort as sorter


def test_mergesort_works(sorting_inputs):
    expected = sorted(sorting_inputs)
    sorter(sorting_inputs)
    assert sorting_inputs == expected
