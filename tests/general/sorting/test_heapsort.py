from general.sorting.heapsort import sort as sorter


def test_heapsort_works(sorting_inputs):
    expected = sorted(sorting_inputs)
    sorter(sorting_inputs)
    assert sorting_inputs == expected
