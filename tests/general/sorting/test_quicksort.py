from general.sorting.quicksort import sort as sorter


def test_quicksort_works(sorting_inputs):
    expected = sorted(sorting_inputs)
    sorter(sorting_inputs)
    assert sorting_inputs == expected
