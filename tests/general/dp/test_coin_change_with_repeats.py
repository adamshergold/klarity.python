from dataclasses import dataclass

import pytest

from general.dp.coin_change_with_repeats import Solution


@dataclass(frozen=True)
class TestCase:
    coins: list[int]
    target: int
    expected: int


def case_generator():
    yield TestCase([], 0, 1)
    yield TestCase([], 1, 0)
    yield TestCase([1, 2, 3], 4, 4)
    yield TestCase([2, 5, 3, 6], 10, 5)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().numberOfWays(test_case.coins, test_case.target)
    assert actual == test_case.expected
