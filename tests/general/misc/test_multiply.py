from dataclasses import dataclass

import pytest

from general.misc.multiply import Solution


@dataclass(frozen=True)
class TestCase:
    a: int
    b: int
    expected: int


def case_generator():
    yield TestCase(2, 0, 0)
    yield TestCase(0, 2, 0)
    yield TestCase(5, 4, 20)
    yield TestCase(5, 6, 30)
    yield TestCase(5, 7, 35)
    yield TestCase(9, 9, 81)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().multiply(test_case.a, test_case.b)
    assert actual == test_case.expected
