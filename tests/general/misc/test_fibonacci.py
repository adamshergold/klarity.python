from dataclasses import dataclass

import pytest

from general.misc.fibonacci import Solution


@dataclass(frozen=True)
class TestCase:
    n: int
    expected: int


def case_generator():
    yield TestCase(1, 1)
    yield TestCase(2, 1)
    yield TestCase(3, 2)
    yield TestCase(4, 3)
    yield TestCase(5, 5)
    yield TestCase(6, 8)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution_1(test_case):
    actual = Solution().fib_1(test_case.n)
    assert actual == test_case.expected


def test_solution_2(test_case):
    actual = Solution().fib_2(test_case.n)
    assert actual == test_case.expected
