from dataclasses import dataclass
import pytest

from geeksforgeeks.sort_array_of_012s import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: list[int]


def case_generator():
    yield TestCase([], [])
    yield TestCase([0, 2, 1, 2, 0], [0, 0, 1, 2, 2])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    expected = sorted(test_case.arr)
    # annoying this one is expected to sort in-place
    Solution().sort012(test_case.arr, len(test_case.arr))
    assert test_case.arr == expected
