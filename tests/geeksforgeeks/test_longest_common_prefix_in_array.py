from dataclasses import dataclass
import pytest

from geeksforgeeks.longest_common_prefix_in_array import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: str


def case_generator():
    yield TestCase([], "-1")
    yield TestCase(["geeksforgeeks", "geeks", "geek", "geezer"], "gee")
    yield TestCase(["hello", "world"], "-1")


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().longestCommonPrefix(test_case.arr, len(test_case.arr))
    assert actual == test_case.expected
