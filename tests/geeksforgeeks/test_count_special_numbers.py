from dataclasses import dataclass
import pytest

from geeksforgeeks.count_special_numbers import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([2, 3, 6], 1)
    yield TestCase([5, 5, 5, 5], 4)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().countSpecialNumbers(len(test_case.arr), test_case.arr)
    assert actual == test_case.expected
