from dataclasses import dataclass
import pytest

from geeksforgeeks.total_cuts import Solution


@dataclass(frozen=True)
class TestCase:
    a: list[int]
    k: int
    expected: int


def case_generator():
    yield TestCase([1, 2, 3], 3, 2)
    yield TestCase([1, 2, 3, 4, 5], 5, 3)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().totalCuts(len(test_case.a), test_case.k, test_case.a)
    assert actual == test_case.expected
