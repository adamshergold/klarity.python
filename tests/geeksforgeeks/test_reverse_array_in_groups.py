from dataclasses import dataclass
import pytest

from geeksforgeeks.reverse_array_in_groups import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    k: int
    expected: int


def case_generator():
    yield TestCase([], 1, [])
    yield TestCase([1], 1, [1])
    yield TestCase([1, 2], 1, [1, 2])
    yield TestCase([1, 2, 3, 4, 5], 3, [3, 2, 1, 5, 4])
    yield TestCase([5, 6, 8, 9], 3, [8, 6, 5, 9])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    Solution().reverseInGroups(test_case.arr, len(test_case.arr), test_case.k)
    assert test_case.arr == test_case.expected
