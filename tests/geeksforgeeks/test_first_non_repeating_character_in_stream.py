from dataclasses import dataclass
import pytest

from geeksforgeeks.first_non_repeating_character_in_stream import Solution


@dataclass(frozen=True)
class TestCase:
    A: str
    expected: str


def case_generator():
    yield TestCase("aabc", "a#bb")
    yield TestCase("zz", "z#")
    yield TestCase("hrqcvsvszpsjammdrw", "hhhhhhhhhhhhhhhhhh")


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().FirstNonRepeating(test_case.A)
    assert actual == test_case.expected
