from dataclasses import dataclass
import pytest

from geeksforgeeks.longest_palindrome import Solution


@dataclass(frozen=True)
class TestCase:
    S: str
    expected: str


def case_generator():
    # yield TestCase("aaaabbaa", "aabbaa")
    yield TestCase("abc", "a")


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().longestPalin(test_case.S)
    assert actual == test_case.expected
