from dataclasses import dataclass
import pytest

from geeksforgeeks.longest_common_substring import Solution


@dataclass(frozen=True)
class TestCase:
    X: str
    Y: str
    expected: int


def case_generator():
    yield TestCase("GeeksforGeeks", "Geeks", 5)
    yield TestCase("abcdxyz", "xyzabcd", 4)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().longestCommonSubstring(test_case.X, test_case.Y)
    assert actual == test_case.expected
