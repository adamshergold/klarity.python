from dataclasses import dataclass
import pytest

from geeksforgeeks.majority_element import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([], -1)
    yield TestCase([1], 1)
    yield TestCase([1, 2], -1)
    yield TestCase([1, 2, 3], -1)
    yield TestCase([3, 1, 3, 3, 2], 3)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().majorityElement(test_case.arr, len(test_case.arr))
    assert actual == test_case.expected


def test_1():
    with open('tests/geeksforgeeks/test_majority_element.1.txt', 'tr') as test_file:
        n = int(test_file.readline().strip())
        vs = list(map(int, test_file.readline().split(' ')))
        assert Solution().majorityElement(vs, n) == 1
