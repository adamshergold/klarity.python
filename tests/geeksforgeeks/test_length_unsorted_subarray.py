from dataclasses import dataclass
import pytest

from geeksforgeeks.length_unsorted_subarray import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: list[int]


def case_generator():
    yield TestCase([10, 12, 20, 30, 25, 40, 32, 31, 35, 50, 60], [3, 8])
    yield TestCase(
        [1, 4, 4, 5, 7, 9, 9, 10, 10, 11, 12, 13, 13, 15, 15, 16, 18,
         20, 21, 22, 22, 23, 24, 25, 25, 27, 28, 29, 29,
         30, 32, 33, 34, 34, 54, 39, 37, 51, 49, 43, 49, 52, 37, 42,
         42, 54, 40, 51, 55, 45, 37, 54, 53, 42, 39, 55, 56,
         58, 59, 61, 62, 62, 63, 64, 65, 68, 68, 70, 72, 72, 73, 73,
         74, 76, 77, 78, 79, 81, 81, 82, 82, 83, 83, 85, 85,
         85, 85, 86, 90, 91, 91, 91, 91, 91, 92, 94, 99, 100], [34, 54])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().printUnsorted(test_case.arr, len(test_case.arr))
    assert actual == test_case.expected
