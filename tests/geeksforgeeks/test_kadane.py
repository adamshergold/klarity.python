from dataclasses import dataclass
import pytest

from geeksforgeeks.kadane import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([], -1)
    yield TestCase([1, 2, 3, -2, 5], 9)
    yield TestCase([-1, -2, -3, -4], -1)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().maxSubArraySum(test_case.arr, len(test_case.arr))
    assert actual == test_case.expected
