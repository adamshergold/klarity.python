from dataclasses import dataclass
import pytest

from geeksforgeeks.find_duplicates_in_an_array import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([0, 3, 1, 2], [-1])
    yield TestCase([2, 3, 1, 2, 3], [2, 3])
    yield TestCase([13, 9, 25, 1, 1, 0, 22, 13, 22, 20, 3, 8, 11, 25, 10,
                    3, 15, 11, 19, 20, 2, 4, 25, 14, 23, 14],
                   [1, 3, 11, 13, 14, 20, 22, 25])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().duplicates(test_case.arr, len(test_case.arr))
    assert actual == test_case.expected
