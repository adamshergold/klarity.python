from dataclasses import dataclass
import pytest

from geeksforgeeks.min_swaps_to_sort import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([2, 8, 5, 4], 1)
    yield TestCase([10, 19, 6, 3, 5], 2)
    yield TestCase([1, 2, 3], 0)
    yield TestCase([7, 16, 14, 17, 6, 9, 5, 3, 18], 5)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().minSwaps(test_case.arr)
    assert actual == test_case.expected
