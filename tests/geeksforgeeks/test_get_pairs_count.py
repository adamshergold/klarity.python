from dataclasses import dataclass
import pytest

from geeksforgeeks.get_pairs_count import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    k: int
    expected: int


def case_generator():
    yield TestCase([], 0, 0)
    yield TestCase([1, 5, 7, 1], 6, 2)
    yield TestCase([1, 1, 1, 1], 2, 6)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().getPairsCount(test_case.arr, len(test_case.arr), test_case.k)
    assert test_case.expected == actual
