from dataclasses import dataclass
import pytest

from geeksforgeeks.minimum_swaps_to_sort import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    expected: int


def case_generator():
    # yield TestCase([2, 8, 5, 4], 1)
    # yield TestCase([10, 19, 6, 3, 5], 2)
    yield TestCase([8, 3, 14, 17, 15, 1, 12], 4)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().minSwaps(test_case.nums)
    assert actual == test_case.expected
