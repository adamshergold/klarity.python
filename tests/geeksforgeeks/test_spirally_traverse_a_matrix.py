from dataclasses import dataclass
import pytest

from geeksforgeeks.spirally_traverse_a_matrix import Solution


@dataclass(frozen=True)
class TestCase:
    matrix: list[list[int]]
    r: int
    c: int
    expected: list[int]


def case_generator():
    yield TestCase([[1]], 1, 1, [1])

    yield TestCase([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]], 3, 4,
                   [1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7])

    yield TestCase([[29, 9,  13],
                    [10, 12, 4],
                    [6,  20, 19],
                    [25, 1,  28]],
                   4, 3,
                   [29, 9, 13, 4, 19, 28, 1, 25, 6, 10, 12, 20])

    yield TestCase([[9, 54],
                    [33, 58],
                    [88, 45],
                    [57, 9],
                    [95, 98],
                    [14, 53],
                    [46, 65],
                    [71, 54],
                    [52, 2],
                    [77, 67]],
                   10, 2,
                   [9, 54, 58, 45, 9, 98, 53, 65, 54, 2, 67, 77, 52, 71, 46, 14, 95, 57, 88, 33])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().spirallyTraverse(test_case.matrix, test_case.r, test_case.c)
    assert actual == test_case.expected
