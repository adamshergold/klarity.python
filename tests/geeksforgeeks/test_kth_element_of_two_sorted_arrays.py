from dataclasses import dataclass
import pytest

from geeksforgeeks.kth_element_of_two_sorted_arrays import Solution


@dataclass(frozen=True)
class TestCase:
    arr1: list[int]
    arr2: list[int]
    k: int
    expected: int


def case_generator():
    yield TestCase([2, 3, 6, 7, 9], [1, 4, 8, 10], 5, 6)
    yield TestCase([100, 112, 256, 349, 770], [72, 86, 113, 119, 265, 445, 892], 7, 256)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution_0(test_case):
    actual = Solution().kthElement_0(
        test_case.arr1,
        test_case.arr2,
        len(test_case.arr1),
        len(test_case.arr2),
        test_case.k)
    assert actual == test_case.expected
