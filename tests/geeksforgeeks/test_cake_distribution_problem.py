from dataclasses import dataclass
import pytest

from geeksforgeeks.cake_distribution_problem import Solution


@dataclass(frozen=True)
class TestCase:
    sweetness: list[int]
    k: int
    expected: int


def case_generator():
    yield TestCase([10], 0, 10)
    yield TestCase([6, 3, 2, 8, 7, 5], 2, 9)
    yield TestCase([6, 3, 2, 8, 7, 5], 3, 5)
    yield TestCase([1, 2, 4, 7, 3, 6, 9], 3, 7)
    yield TestCase([1, 2, 4, 7, 3, 6, 9], 4, 3)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().maxSweetness(test_case.sweetness, len(test_case.sweetness), test_case.k)
    assert actual == test_case.expected
