from dataclasses import dataclass
import pytest

from geeksforgeeks.binary_modulo import Solution


@dataclass(frozen=True)
class TestCase:
    s: str
    m: int
    expected: int


def case_generator():
    yield TestCase("101", 2, 1)
    yield TestCase("1000", 4, 0)
    yield TestCase("10000000", 9, 2)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().modulo(test_case.s, test_case.m)
    assert actual == test_case.expected
