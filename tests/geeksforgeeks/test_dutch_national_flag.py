from dataclasses import dataclass
import pytest

from geeksforgeeks.dutch_national_flag import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: list[int]


def case_generator():
    yield TestCase([0, 2, 1, 2, 0], [0, 0, 1, 2, 2])
    yield TestCase([0, 1, 0], [0, 0, 1])
    yield TestCase([], [])
    yield TestCase([0], [0])
    yield TestCase([1], [1])
    yield TestCase([2], [2])
    yield TestCase([2, 1, 0], [0, 1, 2])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution_dp(test_case):
    Solution().sort012(test_case.arr, len(test_case.arr))
    assert test_case.arr == test_case.expected
