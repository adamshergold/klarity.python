from dataclasses import dataclass
import pytest

from geeksforgeeks.largest_subarray_with_zero_sum import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([15, -2, 2, -8, 1, 7, 10, 23], 5)
    yield TestCase([-1, 1, -1, 1], 4)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().maxLen(len(test_case.arr), test_case.arr)
    assert actual == test_case.expected
