from dataclasses import dataclass
import pytest

from geeksforgeeks.swapping_pairs_make_sum_equal import Solution


@dataclass(frozen=True)
class TestCase:
    a: list[int]
    b: list[int]
    expected: int


def case_generator():
    yield TestCase([], [], -1)
    yield TestCase([1], [2], -1)
    yield TestCase([4, 1, 2, 1, 1, 2], [3, 6, 3, 3], 1)
    yield TestCase([5, 7, 4, 6], [1, 2, 3, 8], 1)
    yield TestCase([1, 2, 3, 8], [5, 7, 4, 6], 1)
    yield TestCase([1, 2, 3, 4], [4, 3, 2, 1], 1)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().findSwapValues(test_case.a, len(test_case.a), test_case.b, len(test_case.b))
    assert actual == test_case.expected


def test_1():
    with open('tests/geeksforgeeks/test_swapping_pairs_make_sum_equal.input1.txt', 'tr') \
            as test_file:
        test_file.readline()
        a = list(map(int, test_file.readline().split(' ')))
        b = list(map(int, test_file.readline().split(' ')))
        actual = Solution().findSwapValues(a, len(a), b, len(b))
        assert actual == -1
