from dataclasses import dataclass
import pytest

from geeksforgeeks.make_palindrome import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[str]
    expected: bool


def case_generator():
    yield TestCase(["djfh", "gadt", "hfjd", "tdag"], True)
    yield TestCase(["jhjdf", "sftas", "fgsdf"], False)
    yield TestCase(["aabaa", "pqrqp"], False)
    yield TestCase(["aaaa", "aaaa", "aaaa", "aaaa"], True)
    yield TestCase(["ghdas", "adsda", "sadhg"], True)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().makePalindrome(len(test_case.arr), test_case.arr)
    assert actual == test_case.expected
