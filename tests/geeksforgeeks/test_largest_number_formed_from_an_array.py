from dataclasses import dataclass
import pytest

from geeksforgeeks.largest_number_formed_from_an_array import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: str


def case_generator():
    yield TestCase([3, 30, 34, 5, 9], "9534330")
    yield TestCase([54, 546, 548, 60], "6054854654")


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().printLargest(test_case.arr)
    assert actual == test_case.expected
