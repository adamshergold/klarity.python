from dataclasses import dataclass
import pytest

from geeksforgeeks.segregate_01s import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: list[int]


def case_generator():
    yield TestCase([0, 0, 1, 1, 0], [0, 0, 0, 1, 1])
    yield TestCase([1, 1, 1, 1], [1, 1, 1, 1])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution_dp(test_case):
    Solution().segregate0and1(test_case.arr, len(test_case.arr))
    assert test_case.arr == test_case.expected
