from dataclasses import dataclass
import pytest

from geeksforgeeks.good_subtrees import Solution, Node


@dataclass(frozen=True)
class TestCase:
    tree: Node
    k: int
    expected: int


def case_generator():
    yield TestCase(Node(1, Node(2, Node(3)), Node(2, Node(5), Node(4))), 2, 4)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().goodSubtrees(test_case.tree, test_case.k)
    assert test_case.expected == actual
