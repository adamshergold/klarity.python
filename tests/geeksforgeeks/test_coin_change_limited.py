from dataclasses import dataclass
import pytest

from geeksforgeeks.coin_change_limited import Solution


@dataclass(frozen=True)
class TestCase:
    coins: list[int]
    k: int
    target: int
    expected: bool


def case_generator():
    yield TestCase([1, 10, 5, 8, 6], 3, 11, True)
    yield TestCase([7, 2, 4], 5, 25, True)
    yield TestCase([9, 5, 3, 2, 9, 4, 5, 5, 9, 1], 8, 2, False)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().makeChanges(len(test_case.coins),
                                    test_case.k, test_case.target, test_case.coins)
    assert actual == test_case.expected
