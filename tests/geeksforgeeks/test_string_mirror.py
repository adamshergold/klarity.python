from dataclasses import dataclass
import pytest

from geeksforgeeks.string_mirror import Solution


@dataclass(frozen=True)
class TestCase:
    s: str
    expected: str


def case_generator():
    yield TestCase("bvdfndkn", "bb")
    yield TestCase("casd", "caac")
    yield TestCase("", "")
    yield TestCase("dbbhbb", "dbbbbd")
    yield TestCase("jjfupkravhthvqfbadrkqg", "jj")


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().stringMirror(test_case.s)
    assert actual == test_case.expected
