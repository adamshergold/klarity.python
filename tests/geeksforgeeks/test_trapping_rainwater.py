from dataclasses import dataclass
import pytest

from geeksforgeeks.trapping_rainwater import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([3, 0, 0, 2, 0, 4], 10)
    yield TestCase([7, 4, 0, 9], 10)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().trappingWater(test_case.arr, len(test_case.arr))
    assert actual == test_case.expected
