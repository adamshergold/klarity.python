from dataclasses import dataclass
import pytest

from geeksforgeeks.equal_left_and_right_subarray_sum import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([], -1)
    yield TestCase([1], 1)
    yield TestCase([1, 2, 3], -1)
    yield TestCase([1, 3, 5, 2, 2], 3)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().equalSum(test_case.arr, len(test_case.arr))
    assert actual == test_case.expected
