from dataclasses import dataclass
import pytest

from geeksforgeeks.stock_buy_and_sell import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: list[int]


def case_generator():
    yield TestCase([100, 180, 260, 310, 40, 535, 695], [[0, 3], [4, 6]])
    yield TestCase([4, 2, 2, 2, 4], [[3, 4]])
    yield TestCase([4, 3, 2, 1], [])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().stockBuySell(test_case.arr, len(test_case.arr))
    assert actual == test_case.expected
