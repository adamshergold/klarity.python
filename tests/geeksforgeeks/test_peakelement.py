from dataclasses import dataclass
import pytest

from geeksforgeeks.peak_element import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([], -1)
    yield TestCase([1], 0)
    yield TestCase([1, 2], 1)
    yield TestCase([1, 2, 3], 2)
    yield TestCase([3, 4, 2], 1)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().peakElement(test_case.arr, len(test_case.arr))
    assert actual == test_case.expected
