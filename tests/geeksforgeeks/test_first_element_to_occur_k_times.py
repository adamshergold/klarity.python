from dataclasses import dataclass
import pytest

from geeksforgeeks.first_element_to_occur_k_times import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    k: int
    expected: int


def case_generator():
    yield TestCase([], 2, -1)
    yield TestCase([2], 2, -1)
    yield TestCase([2, 3, 1, 3, 1], 2, 3)
    yield TestCase([1, 7, 4, 3, 4, 8, 7], 2, 4)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().firstElementKTime(test_case.arr, len(test_case.arr), test_case.k)
    assert actual == test_case.expected
