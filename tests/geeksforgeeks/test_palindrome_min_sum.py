from dataclasses import dataclass
import pytest

from geeksforgeeks.palindrome_min_sum import Solution


@dataclass(frozen=True)
class TestCase:
    s: str
    expected: int


def case_generator():
    yield TestCase("a???c??c????", 4)
    yield TestCase("a???c??c???c", -1)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().minimumSum(test_case.s)
    assert actual == test_case.expected
