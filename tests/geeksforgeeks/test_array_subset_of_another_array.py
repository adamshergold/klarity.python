from dataclasses import dataclass
import pytest

from geeksforgeeks.array_subset_of_another_array import isSubset


@dataclass(frozen=True)
class TestCase:
    a1: list[int]
    a2: list[int]
    expected: str


def case_generator():
    yield TestCase([], [], "Yes")
    yield TestCase([1], [], "Yes")
    yield TestCase([1], [2], "No")
    yield TestCase([11, 1, 13, 21, 3, 7], [11, 3, 7, 1], "Yes")
    yield TestCase([1, 2, 3, 4, 5, 6], [1, 2, 4], "Yes")
    yield TestCase([1, 2, 3, 4, 5, 6, 7, 8], [1, 2, 3, 1], "No")


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = isSubset(test_case.a1, test_case.a2, len(test_case.a1), len(test_case.a2))
    assert actual == test_case.expected
