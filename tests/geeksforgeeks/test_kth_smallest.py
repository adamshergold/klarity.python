from dataclasses import dataclass
import pytest

from geeksforgeeks.kth_smallest import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    k: int
    expected: int


def case_generator():
    yield TestCase([12, 5, 787, 1, 23], 2, 5)
    yield TestCase([], 0, -1)
    yield TestCase([7, 10, 4, 3, 20, 15], 3, 7)
    yield TestCase([7, 10, 4, 20, 15], 4, 15)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().kthSmallest(test_case.arr, 0, len(test_case.arr)-1, test_case.k)
    assert actual == test_case.expected


def test_1():
    with open('tests/geeksforgeeks/test_kth_smallest.1.txt', 'tr') as test_file:
        n = int(test_file.readline().strip())
        vs = list(map(int, test_file.readline().split(' ')))
        k = int(test_file.readline().strip())
        assert Solution().kthSmallest(vs, 0, n-1, k) == 26558
