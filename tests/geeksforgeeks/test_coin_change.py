from dataclasses import dataclass
import pytest

from geeksforgeeks.coin_change import Solution


@dataclass(frozen=True)
class TestCase:
    coins: list[int]
    sum: int
    expected: int


def case_generator():
    yield TestCase([], -1, 0)
    yield TestCase([], 1, 0)
    yield TestCase([], 0, 1)
    yield TestCase([2], 1, 0)
    yield TestCase([2], 2, 1)
    yield TestCase([2], 4, 1)
    yield TestCase([1, 2, 3], 4, 4)
    yield TestCase([2, 3, 5, 6], 10, 5)
    yield TestCase([1, 2, 3, 4], 4, 5)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().count(test_case.coins, len(test_case.coins), test_case.sum)
    assert actual == test_case.expected


def test_solution_0(test_case):
    actual = Solution().count_0(test_case.coins, len(test_case.coins), test_case.sum)
    assert actual == test_case.expected
