from dataclasses import dataclass
import pytest

from geeksforgeeks.convert_array_into_zigzag import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: list[int]


def case_generator():
    yield TestCase([], [])
    yield TestCase([1], [1])
    yield TestCase([1, 2], [1, 2])
    yield TestCase([2, 1], [1, 2])
    yield TestCase([4, 3, 7, 8, 6, 2, 1], [3, 7, 4, 8, 2, 6, 1])
    yield TestCase([1, 4, 3, 2], [1, 4, 2, 3])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    Solution().zigZag(test_case.arr, len(test_case.arr))
    assert test_case.arr == test_case.expected
