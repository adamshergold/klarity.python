from dataclasses import dataclass
import pytest

from geeksforgeeks.minimum_integer import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([3], 3)
    yield TestCase([1, 3, 2], 2)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().minimumInteger(len(test_case.arr), test_case.arr)
    assert actual == test_case.expected
