from dataclasses import dataclass
import pytest

from geeksforgeeks.is_subset_sum import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    sum: int
    expected: bool


def case_generator():
    yield TestCase([], 0, True)
    yield TestCase([], 1, False)
    yield TestCase([3, 34, 4, 12, 5, 2], 9, True)
    yield TestCase([3, 34, 4, 12, 5, 2], 30, False)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().isSubsetSum(len(test_case.arr), test_case.arr, test_case.sum)
    assert test_case.expected == actual
