from dataclasses import dataclass
import pytest

from geeksforgeeks.minimze_the_heights_2 import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    k: int
    expected: int


def case_generator():
    yield TestCase([1, 5, 8, 10], 2, 5)
    yield TestCase([3, 9, 12, 16, 20], 3, 11)
    yield TestCase([2, 6, 3, 4, 7, 2, 10, 3, 2, 1], 5, 7)
    yield TestCase([5, 5, 8, 6, 4, 10, 3, 8, 9, 10], 5, 7)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().getMinDiff(test_case.arr, len(test_case.arr), test_case.k)
    assert actual == test_case.expected
