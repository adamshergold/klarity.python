from dataclasses import dataclass
import pytest

from geeksforgeeks.leaders_in_array import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([], [])
    yield TestCase([16, 17, 4, 3, 5, 2], [17, 5, 2])
    yield TestCase([1, 2, 3, 4, 0], [4, 0])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().leaders(test_case.arr, len(test_case.arr))
    assert actual == test_case.expected
