from dataclasses import dataclass
import pytest

from geeksforgeeks.sort_array_according_to_the_other import Solution


@dataclass(frozen=True)
class TestCase:
    a1: list[int]
    a2: list[int]
    expected: list[int]


def case_generator():
    yield TestCase([2, 1, 2, 5, 7, 1, 9, 3, 6, 8, 8],
                   [2, 1, 8, 3], [2, 2, 1, 1, 8, 8, 3, 5, 6, 7, 9])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().relativeSort(test_case.a1,
                                     len(test_case.a1),
                                     test_case.a2,
                                     len(test_case.a2))

    assert actual == test_case.expected
