from dataclasses import dataclass
import pytest

from geeksforgeeks.maximum_no_of_1s_row import Solution


@dataclass(frozen=True)
class TestCase:
    mat: list[list[int]]
    n: int
    m: int
    expected: int


def case_generator():
    yield TestCase([[0, 1, 1, 1], [0, 0, 1, 1], [0, 0, 1, 1]], 3, 4, 0)
    yield TestCase([[0, 1], [1, 1]], 2, 2, 1)
    yield TestCase([[1]], 1, 1, 0)
    yield TestCase([[0]], 1, 1, 0)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().maxOnes(test_case.mat, test_case.n, test_case.m)
    assert actual == test_case.expected
