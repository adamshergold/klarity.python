from dataclasses import dataclass
import pytest

from geeksforgeeks.zero_sum_subarrays import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([0, 0, 5, 5, 0, 0], 6)
    yield TestCase([6, -1, -3, 4, -2, 2, 4, 6, -12, -7], 4)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().findSubArrays(test_case.arr, len(test_case.arr))
    assert actual == test_case.expected


def test_solution_0(test_case):
    actual = Solution().findSubArrays_0(test_case.arr, len(test_case.arr))
    assert actual == test_case.expected
