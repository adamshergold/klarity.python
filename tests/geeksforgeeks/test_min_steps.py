from dataclasses import dataclass
import pytest

from geeksforgeeks.min_steps import Solution


@dataclass(frozen=True)
class TestCase:
    s: str
    expected: int


def case_generator():
    yield TestCase("bbaaabb", 2)
    yield TestCase("aababaa", 3)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().minSteps(test_case.s)
    assert actual == test_case.expected
