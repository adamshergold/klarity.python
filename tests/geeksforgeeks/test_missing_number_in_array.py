from dataclasses import dataclass
import pytest

from geeksforgeeks.missing_number_in_array import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([], 1)
    yield TestCase([1], 2)
    yield TestCase([2], 1)
    yield TestCase([1, 2, 3, 5], 4)
    yield TestCase([6, 1, 2, 8, 3, 4, 7, 10, 5], 9)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().MissingNumber(test_case.arr, len(test_case.arr)+1)
    assert actual == test_case.expected
