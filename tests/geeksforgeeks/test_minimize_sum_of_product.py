from dataclasses import dataclass
import pytest

from geeksforgeeks.minimize_sum_of_product import Solution


@dataclass(frozen=True)
class TestCase:
    a: list[int]
    b: list[int]
    expected: int


def case_generator():
    yield TestCase([3, 1, 1], [6, 5, 4], 23)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().minValue(test_case.a, test_case.b, len(test_case.a))
    assert actual == test_case.expected
