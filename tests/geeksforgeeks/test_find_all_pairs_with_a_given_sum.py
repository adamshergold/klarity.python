from dataclasses import dataclass
import pytest

from geeksforgeeks.find_all_pairs_with_a_given_sum import Solution


@dataclass(frozen=True)
class TestCase:
    a: list[int]
    b: list[int]
    x: int
    expected: list[list[int]]


def case_generator():
    yield TestCase([], [], 1, [])
    yield TestCase([1, 2], [1, 2], 3, [[1, 2], [2, 1]])
    yield TestCase([1, 2, 4, 5, 7], [5, 6, 3, 4, 8], 9, [[1, 8], [4, 5], [5, 4]])
    yield TestCase([-1, -2, 4, -6, 5, 7], [6, 3, 4, 0], 8, [[4, 4], [5, 3]])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().allPairs(test_case.a,
                                 test_case.b,
                                 len(test_case.a),
                                 len(test_case.b),
                                 test_case.x)
    assert actual == test_case.expected


def test_1():
    with open('tests/geeksforgeeks/test_find_all_pairs_with_a_given_sum.1.output.txt', 'tr') \
            as output_file:
        expected = [list(map(int, s.strip().split(' '))) for s in output_file.readline().split(',')]

    with open('tests/geeksforgeeks/test_find_all_pairs_with_a_given_sum.1.input.txt', 'tr') \
            as input_file:
        _, _, s_x = input_file.readline().strip().split(' ')
        a = list(map(int, input_file.readline().strip().split(' ')))
        b = list(map(int, input_file.readline().strip().split(' ')))
        actual = Solution().allPairs(a, b, len(a), len(b), int(s_x))
        assert actual == expected
