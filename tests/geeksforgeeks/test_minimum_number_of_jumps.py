from dataclasses import dataclass
import pytest

from geeksforgeeks.minimum_number_of_jumps import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([], -1)
    yield TestCase([0], -1)
    yield TestCase([2, 1, 0, 3], -1)
    yield TestCase([1, 3, 5, 8, 9, 2, 6, 7, 6, 8, 9], 3)
    yield TestCase([1, 4, 3, 2, 6, 7], 2)
    yield TestCase([1, 3, 5, 8, 9, 2, 6, 7, 6, 8, 9], 3)
    yield TestCase([2, 3, 1, 1, 2, 4, 2, 0, 1, 1], 4)
    yield TestCase([9, 10, 1, 2, 3, 4, 8, 0, 0, 0, 0, 0, 0, 0, 1], 2)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().minJumps(test_case.arr, len(test_case.arr))
    assert actual == test_case.expected
