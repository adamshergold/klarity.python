from dataclasses import dataclass
import pytest

from geeksforgeeks.overlapping_intervals import Solution


@dataclass(frozen=True)
class TestCase:
    intervals: list[list[int]]
    expected: list[list[int]]


def case_generator():
    yield TestCase([[1, 3], [2, 4], [6, 8], [9, 10]], [[1, 4], [6, 8], [9, 10]])
    yield TestCase([[6, 8], [1, 9], [2, 4], [4, 7]], [[1, 9]])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().overlappedInterval(test_case.intervals)
    assert actual == test_case.expected
