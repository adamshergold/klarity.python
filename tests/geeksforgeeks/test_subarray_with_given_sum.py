from dataclasses import dataclass
import pytest

from geeksforgeeks.subarray_with_given_sum import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    s: int
    expected: int


def case_generator():
    yield TestCase([], 0, [-1])
    yield TestCase([1, 2, 3, 7, 5], 12, [2, 4])
    yield TestCase([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 15, [1, 5])
    yield TestCase([10, 9, 8], 1, [-1])
    yield TestCase([10, 9, 8], 8, [3, 3])
    yield TestCase([10, 9, 8], 28, [-1])
    yield TestCase([142, 112, 54, 69, 148, 45, 63, 158, 38, 60, 124, 142, 130, 179,
                    117, 36, 191, 43, 89, 107, 41, 143, 65, 49, 47, 6, 91, 130, 171,
                    151, 7, 102, 194, 149, 30, 24, 85, 155, 157, 41, 167,
                    177, 132, 109, 145, 40, 27, 124, 138, 139, 119, 83, 130, 142,
                    34, 116, 40, 59, 105, 131, 178, 107, 74, 187, 22, 146, 125, 73,
                    71, 30, 178, 174, 98, 113], 665, [-1])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().subArraySum(test_case.arr, len(test_case.arr), test_case.s)
    assert test_case.expected == actual
