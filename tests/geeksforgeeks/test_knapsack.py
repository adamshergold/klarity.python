from dataclasses import dataclass
import pytest

from geeksforgeeks.knapsack import Solution


@dataclass(frozen=True)
class TestCase:
    values: list[int]
    weights: int
    w: int
    expected: int


def case_generator():
    yield TestCase([1, 2, 3], [4, 5, 1], 4, 3)
    yield TestCase([1, 2, 3], [4, 5, 6], 3, 0)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution_recursive_memoised(test_case):
    actual = Solution().knapSack_recursive_memoised(
        test_case.w, test_case.weights,
        test_case.values, len(test_case.values))
    assert actual == test_case.expected


def test_solution_dp(test_case):
    actual = Solution().knapSack_dp(
        test_case.w, test_case.weights, test_case.values, len(test_case.values))
    assert actual == test_case.expected
