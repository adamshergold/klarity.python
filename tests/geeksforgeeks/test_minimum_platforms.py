from dataclasses import dataclass
import pytest

from geeksforgeeks.minimum_platforms import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    dep: list[int]
    expected: int


def case_generator():
    yield TestCase([900], [], -1)
    yield TestCase([900], [910], 1)
    yield TestCase([], [], 0)
    yield TestCase([900, 940, 950, 1100, 1500, 1800], [910, 1200, 1120, 1130, 1900, 2000], 3)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().minimumPlatform(len(test_case.arr), test_case.arr, test_case.dep)
    assert actual == test_case.expected
