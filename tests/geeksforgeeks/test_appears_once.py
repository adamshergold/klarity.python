from dataclasses import dataclass
import pytest

from geeksforgeeks.appears_once import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([1, 1, 2, 2, 3, 3, 4, 50, 50, 65, 65], 4)
    yield TestCase([1, 1, 2, 2, 3], 3)
    yield TestCase([1], 1)
    yield TestCase([1, 1, 2], 2)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().findOnce(test_case.arr, len(test_case.arr))
    assert actual == test_case.expected
