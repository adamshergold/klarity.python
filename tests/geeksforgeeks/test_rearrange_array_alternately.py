from dataclasses import dataclass
import pytest

from geeksforgeeks.rearrange_array_alternately import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: list[int]


def case_generator():
    yield TestCase([1, 2, 3, 4, 5, 6], [6, 1, 5, 2, 4, 3])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    Solution().rearrange(test_case.arr, len(test_case.arr))
    assert test_case.expected == test_case.arr
