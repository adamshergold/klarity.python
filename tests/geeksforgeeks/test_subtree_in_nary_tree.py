from dataclasses import dataclass
import pytest

from geeksforgeeks.subtree_in_nary_tree import Solution


@dataclass(frozen=True)
class TestCase:
    s: str
    expected: int


def case_generator():
    yield TestCase("1 N 2 2 3 N 4 N 4 4 3 N N N N N", 2)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):

    root = Solution().createRoot(test_case.s)
    actual = Solution().duplicateSubtreeNaryTree(root)
    assert actual == test_case.expected
