from dataclasses import dataclass
import pytest

from geeksforgeeks.binary_search import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    k: int
    expected: int


def case_generator():
    yield TestCase([], 4, -1)
    yield TestCase([1], 1, 0)
    yield TestCase([1], 2, -1)
    yield TestCase([5, 6], 4, -1)
    yield TestCase([5, 6], 5, 0)
    yield TestCase([5, 6], 6, 1)
    yield TestCase([5, 6], 7, -1)
    yield TestCase([1, 2, 3, 4, 5], 4, 3)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().binarysearch(test_case.arr, len(test_case.arr), test_case.k)
    assert actual == test_case.expected
