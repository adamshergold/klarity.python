from dataclasses import dataclass
import pytest

from geeksforgeeks.dominant_pairs import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([10, 2, 2, 1], 2)
    yield TestCase([10, 8, 2, 1, 1, 2], 5)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().dominantPairs(len(test_case.arr), test_case.arr)
    assert actual == test_case.expected
