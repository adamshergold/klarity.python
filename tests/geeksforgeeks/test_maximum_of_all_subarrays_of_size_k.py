from dataclasses import dataclass
import pytest

from geeksforgeeks.maximum_of_all_subarrays_of_size_k import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    k: int
    expected: list[int]


def case_generator():
    yield TestCase([1, 2, 3, 1, 4, 5, 2, 3, 6], 3, [3, 3, 4, 5, 5, 5, 6])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution_0(test_case):
    actual = Solution().max_of_subarrays_0(test_case.arr, len(test_case.arr), test_case.k)
    assert actual == test_case.expected


def test_solution(test_case):
    actual = Solution().max_of_subarrays(test_case.arr, len(test_case.arr), test_case.k)
    assert actual == test_case.expected
