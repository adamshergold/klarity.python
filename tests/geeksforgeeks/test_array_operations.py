from dataclasses import dataclass
import pytest

from geeksforgeeks.array_operations import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([3, 0, 4, 5], 2)
    yield TestCase([10, 4, 9, 6, 10, 10, 4, 4], -1)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().arrayOperations(len(test_case.arr), test_case.arr)
    assert actual == test_case.expected
