from dataclasses import dataclass
import pytest

from geeksforgeeks.check_if_two_arrays_are_equal import Solution


@dataclass(frozen=True)
class TestCase:
    a: list[int]
    b: list[int]
    expected: bool


def case_generator():
    yield TestCase([], [], True)
    yield TestCase([], [1], False)
    yield TestCase([1], [0], False)
    yield TestCase([1, 2, 5, 4, 0], [2, 4, 5, 0, 1], True)
    yield TestCase([1, 2, 5], [2, 4, 15], False)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().check(test_case.a, test_case.b, len(test_case.a))
    assert actual == test_case.expected
