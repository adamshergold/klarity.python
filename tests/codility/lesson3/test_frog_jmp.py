from dataclasses import dataclass

import pytest

from codility.lesson3.frog_jmp import solution


@dataclass(frozen=True)
class TestCase:
    x: int
    y: int
    d: int
    expected: int


def case_generator():
    yield TestCase(10, 85, 30, 3)
    yield TestCase(0, 10, 5, 2)
    yield TestCase(0, 11, 5, 3)
    yield TestCase(0, 12, 5, 3)
    yield TestCase(0, 13, 5, 3)
    yield TestCase(0, 14, 5, 3)
    yield TestCase(0, 15, 5, 3)
    yield TestCase(0, 16, 5, 4)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = solution(test_case.x, test_case.y, test_case.d)
    assert actual == test_case.expected
