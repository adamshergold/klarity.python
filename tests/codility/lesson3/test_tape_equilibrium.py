from dataclasses import dataclass

import pytest

from codility.lesson3.tape_equlibrium import solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([3, 1, 2, 4, 3], 1)
    yield TestCase([1, 2], 1)
    yield TestCase([5, 2], 3)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = solution(test_case.arr)
    assert actual == test_case.expected
