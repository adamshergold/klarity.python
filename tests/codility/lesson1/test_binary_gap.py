from dataclasses import dataclass
import pytest

from codility.lesson1.binary_gap import Solution


@dataclass(frozen=True)
class TestCase:
    N: int
    expected: int


def case_generator():
    yield TestCase(0, 0)
    yield TestCase(9, 2)
    yield TestCase(20, 1)
    yield TestCase(15, 0)
    yield TestCase(32, 0)
    yield TestCase(1041, 5)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution(test_case.N)
    assert actual == test_case.expected
