from dataclasses import dataclass

import pytest

from codility.lesson2.cyclic_rotation import solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    k: int
    expected: list[int]


def case_generator():
    yield TestCase([], 0, [])
    yield TestCase([], 1, [])
    yield TestCase([3, 8, 9, 7, 6], 3, [9, 7, 6, 3, 8])
    yield TestCase([3, 8, 9, 7, 6], 5, [3, 8, 9, 7, 6])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = solution(test_case.arr, test_case.k)
    assert actual == test_case.expected
