from dataclasses import dataclass

import pytest

from codility.lesson2.odd_occurences_in_array import solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([3], 3)
    yield TestCase([3, 4, 3], 4)
    yield TestCase([3, 3, 4, 4, 4, 3, 3], 4)
    yield TestCase([9, 3, 9, 3, 9, 7, 9], 7)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = solution(test_case.arr)
    assert actual == test_case.expected
