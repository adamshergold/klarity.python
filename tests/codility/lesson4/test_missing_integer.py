from dataclasses import dataclass

import pytest

from codility.lesson4.missing_integer import solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([1, 3, 6, 4, 1, 2], 5)
    yield TestCase([1, 2, 3], 4)
    yield TestCase([-1, -3], 1)
    yield TestCase([-1, 1], 2)
    yield TestCase([2], 1)
    yield TestCase([90, 91, 92, 93], 1)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = solution(test_case.arr)
    assert actual == test_case.expected
