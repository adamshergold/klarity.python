from dataclasses import dataclass

import pytest

from codility.lesson4.frog_river_one import solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    X: int
    expected: int


def case_generator():
    yield TestCase([1, 3, 1, 4, 2, 3, 5, 4], 5, 6)
    yield TestCase([1, 1, 1], 2, -1)
    yield TestCase([1, 3, 2], 3, 2)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = solution(test_case.X, test_case.arr)
    assert actual == test_case.expected
