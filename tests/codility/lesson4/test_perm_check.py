from dataclasses import dataclass

import pytest

from codility.lesson4.perm_check import solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([1], 1)
    yield TestCase([4, 1, 3, 2], 1)
    yield TestCase([4, 1, 3], 0)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = solution(test_case.arr)
    assert actual == test_case.expected
