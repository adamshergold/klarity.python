from dataclasses import dataclass

import pytest

from codility.lesson4.max_counters import solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    n: int
    expected: int


def case_generator():
    yield TestCase([], 0, [])
    yield TestCase([3, 4, 4, 6, 1, 4, 4], 5, [3, 2, 2, 4, 2])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = solution(test_case.n, test_case.arr)
    assert actual == test_case.expected
