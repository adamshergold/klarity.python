from dataclasses import dataclass

import pytest

from codility.lesson5.count_div import solution


@dataclass(frozen=True)
class TestCase:
    a: int
    b: int
    k: int
    expected: int


def case_generator():
    yield TestCase(0, 0, 2, 1)
    yield TestCase(6, 11, 2, 3)
    yield TestCase(5, 11, 2, 3)
    yield TestCase(6, 10, 2, 3)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = solution(test_case.a, test_case.b, test_case.k)
    assert actual == test_case.expected
