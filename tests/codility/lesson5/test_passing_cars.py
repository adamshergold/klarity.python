from dataclasses import dataclass

import pytest

from codility.lesson5.passing_cars import solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    expected: int


def case_generator():
    yield TestCase([0, 1, 0, 1, 1], 5)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = solution(test_case.arr)
    assert actual == test_case.expected


def test_out_of_range():
    actual = solution([0] * 1000000 + [1] * 1000000)
    assert actual == -1
