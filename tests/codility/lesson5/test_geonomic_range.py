from dataclasses import dataclass

import pytest

from codility.lesson5.geonomic_range import solution


@dataclass(frozen=True)
class TestCase:
    p: list[int]
    q: list[int]
    s: str
    expected: list[int]


def case_generator():
    yield TestCase([2, 5, 0], [4, 5, 6], "CAGCCTA", [2, 4, 1])
    yield TestCase([0, 0, 1], [0, 1, 1], "AC", [1, 1, 2])
    yield TestCase([0], [2], "GGG", [3])
    yield TestCase([0], [2], "TTT", [4])
    yield TestCase([0], [0], "X", [-1])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = solution(test_case.s, test_case.p, test_case.q)
    assert actual == test_case.expected
