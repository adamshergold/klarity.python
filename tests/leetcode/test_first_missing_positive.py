from dataclasses import dataclass

import pytest

from leetcode.first_missing_positive import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    expected: int


def case_generator():
    yield TestCase([1], 2)
    yield TestCase([1, 1], 2)
    yield TestCase([1, 2, 0], 3)
    yield TestCase([3, 4, -1, 1], 2)
    yield TestCase([-1, -2, -3], 1)
    yield TestCase([7, 8, 9, 11, 12], 1)
    yield TestCase([-1, 4, 2, 1, 9, 10], 3)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().firstMissingPositive(test_case.nums)
    assert actual == test_case.expected
