from dataclasses import dataclass

import pytest

from leetcode.remove_element import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    val: int
    n: int
    expected: list[int]


def case_generator():
    yield TestCase([3], 3, 0, [])
    yield TestCase([3, 2, 2, 3], 3, 2, [2, 2])
    yield TestCase([0, 1, 2, 2, 3, 0, 4, 2], 2, 5, [0, 1, 3, 0, 4])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().removeElement(test_case.nums, test_case.val)
    assert actual == test_case.n
    assert test_case.nums[:actual] == test_case.expected
