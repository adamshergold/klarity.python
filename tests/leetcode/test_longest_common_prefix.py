from dataclasses import dataclass

import pytest

from leetcode.longest_common_prefix import Solution


@dataclass(frozen=True)
class TestCase:
    strs: list[str]
    expected: str


def case_generator():
    yield TestCase(["flower", "flow", "flight"], "fl")
    yield TestCase(["dog", "racecar", "car"], "")
    yield TestCase([""], "")
    yield TestCase([], "")
    yield TestCase(["a"], "a")


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().longestCommonPrefix(test_case.strs)
    assert actual == test_case.expected
