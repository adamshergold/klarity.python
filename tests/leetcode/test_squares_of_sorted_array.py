from dataclasses import dataclass

import pytest

from leetcode.squares_of_sorted_array import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    expected: list[int]


def case_generator():
    yield TestCase([-1], [1])
    yield TestCase([-4, -1, 0, 3, 10], [0, 1, 9, 16, 100])
    yield TestCase([-7, -3, 2, 3, 11], [4, 9, 9, 49, 121])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().sortedSquares(test_case.nums)
    assert actual == test_case.expected
