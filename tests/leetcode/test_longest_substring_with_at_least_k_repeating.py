from dataclasses import dataclass

import pytest

from leetcode.longest_substring_with_at_least_k_repeating import Solution


@dataclass(frozen=True)
class TestCase:
    s: str
    k: int
    expected: int


def case_generator():
    yield TestCase("aaabb", 3, 3)
    yield TestCase("ababbc", 2, 5)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().longestSubstring(test_case.s, test_case.k)
    assert actual == test_case.expected
