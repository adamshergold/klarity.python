from dataclasses import dataclass

import pytest

from leetcode.reverse_integer import Solution


@dataclass(frozen=True)
class TestCase:
    i: str
    expected: str


def case_generator():
    yield TestCase(123, 321)
    yield TestCase(-123, -321)
    yield TestCase(120, 21)
    yield TestCase(pow(2, 31), 0)
    yield TestCase(-pow(2, 31)-1, 0)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().reverse(test_case.i)
    assert actual == test_case.expected
