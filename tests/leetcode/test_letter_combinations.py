from dataclasses import dataclass

import pytest

from leetcode.letter_combinations import Solution


@dataclass(frozen=True)
class TestCase:
    digits: str
    expected: list[int]


def case_generator():
    yield TestCase("23", ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"])
    yield TestCase("", [])
    yield TestCase("2", ["a", "b", "c"])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().letterCombinations(test_case.digits)
    assert actual == test_case.expected
