from dataclasses import dataclass
import ast
import pytest

from leetcode.jump_game import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    expected: bool


def case_generator():
    yield TestCase([2, 3, 1, 1, 4], True)
    yield TestCase([3, 2, 1, 0, 4], False)
    yield TestCase([0], True)
    yield TestCase([0, 1], False)
    yield TestCase([1, 3, 2], True)
    yield TestCase([2, 0], True)
    yield TestCase([2, 0, 0], True)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().canJump(test_case.nums)
    assert actual == test_case.expected


def test_1():
    with open('tests/leetcode/test_jump_game.1.txt', 'tr') as test_file:
        nums = ast.literal_eval(test_file.readline())
        expected = True if test_file.readline().strip() == 'True' else False
        assert Solution().canJump(nums) == expected
