from dataclasses import dataclass

import pytest

from leetcode.container_with_most_water import Solution


@dataclass(frozen=True)
class TestCase:
    heights: list[int]
    expected: int


def case_generator():
    yield TestCase([], 0)
    yield TestCase([1, 8, 6, 2, 5, 4, 8, 3, 7], 49)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().maxArea(test_case.heights)
    assert actual == test_case.expected
