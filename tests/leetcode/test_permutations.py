from dataclasses import dataclass

import pytest

from leetcode.permutations import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    expected: list[list[int]]


def case_generator():
    yield TestCase([42], [[42]])
    yield TestCase([1, 2, 3], [[1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], [3, 2, 1]])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().permute(test_case.nums)
    assert actual == test_case.expected
