from math import isclose
from dataclasses import dataclass
import pytest

from leetcode.pow import Solution


@dataclass(frozen=True)
class TestCase:
    x: float
    n: int
    expected: float


def case_generator():
    yield TestCase(2, 0, 1)
    yield TestCase(2, 10, 1024)
    yield TestCase(2.1, 3, 9.261)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().myPow(test_case.x, test_case.n)
    assert isclose(actual, test_case.expected)
