from dataclasses import dataclass

import pytest

from leetcode.add_two_numbers import Solution, ListNode


@dataclass(frozen=True)
class TestCase:
    l1: list[int]
    l2: list[int]
    expected: list[int]


def case_generator():
    yield TestCase([2, 4, 3], [5, 6, 4], [7, 0, 8])
    yield TestCase([0], [0], [0])
    yield TestCase([9, 9, 9, 9, 9, 9, 9], [9, 9, 9, 9], [8, 9, 9, 9, 0, 0, 0, 1])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):

    l2 = ListNode(test_case.l2[-1])
    for i in range(len(test_case.l2) - 2, -1, -1):
        l2 = ListNode(test_case.l2[i], l2)

    l1 = ListNode(test_case.l1[-1])
    for i in range(len(test_case.l1) - 2, -1, -1):
        l1 = ListNode(test_case.l1[i], l1)

    actual_ptr = Solution().addTwoNumbers(l1, l2)
    actual = []
    while actual_ptr is not None:
        actual.append(actual_ptr.val)
        actual_ptr = actual_ptr.next
    assert actual == test_case.expected
