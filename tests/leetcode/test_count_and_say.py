from dataclasses import dataclass

import pytest

from leetcode.count_and_say import Solution


@dataclass(frozen=True)
class TestCase:
    n: int
    expected: str


def case_generator():
    yield TestCase(1, "1")
    yield TestCase(2, "11")
    yield TestCase(3, "21")
    yield TestCase(4, "1211")


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().countAndSay(test_case.n)
    assert actual == test_case.expected
