from dataclasses import dataclass

import pytest

from leetcode.integer_to_roman import Solution


@dataclass(frozen=True)
class TestCase:
    num: int
    expected: str


def case_generator():
    yield TestCase(0, "")
    yield TestCase(1000, "M")
    yield TestCase(900, "CM")
    yield TestCase(500, "D")
    yield TestCase(400, "CD")
    yield TestCase(300, "CCC")
    yield TestCase(200, "CC")
    yield TestCase(100, "C")
    yield TestCase(90, "XC")
    yield TestCase(80, "LXXX")
    yield TestCase(60, "LX")
    yield TestCase(50, "L")
    yield TestCase(40, "XL")
    yield TestCase(49, "XLIX")
    yield TestCase(30, "XXX")
    yield TestCase(31, "XXXI")
    yield TestCase(3, "III")
    yield TestCase(58, "LVIII")
    yield TestCase(10, "X")
    yield TestCase(11, "XI")
    yield TestCase(14, "XIV")
    yield TestCase(15, "XV")
    yield TestCase(9, "IX")
    yield TestCase(1994, "MCMXCIV")


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().intToRoman(test_case.num)
    assert actual == test_case.expected
