from dataclasses import dataclass

import pytest

from leetcode.coin_change import Solution


@dataclass(frozen=True)
class TestCase:
    coins: list[int]
    amount: int
    expected: int


def case_generator():
    yield TestCase([1, 2, 5], 11, 3)
    yield TestCase([2], 3, -1)
    yield TestCase([], 0, 0)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution_0(test_case):
    actual = Solution().coinChange_0(test_case.coins, test_case.amount)
    assert actual == test_case.expected


def test_solution_1(test_case):
    actual = Solution().coinChange_1(test_case.coins, test_case.amount)
    assert actual == test_case.expected
