from dataclasses import dataclass

import pytest

from leetcode.jump_game_ii import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    expected: int


def case_generator():
    yield TestCase([2, 3, 1, 1, 4], 2)
    yield TestCase([2, 3, 0, 1, 4], 2)
    yield TestCase([0], 0)
    yield TestCase([1, 3, 2], 2)
    yield TestCase([5, 9, 3, 2, 1, 0, 2, 3, 3, 1, 0, 0], 3)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().jump(test_case.nums)
    assert actual == test_case.expected
