from dataclasses import dataclass

import pytest

from leetcode.remove_duplicates_from_sorted_array import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    n: int
    expected: list[int]


def case_generator():
    yield TestCase([1, 1, 2], 2, [1, 2])
    yield TestCase([0, 0, 1, 1, 1, 2, 2, 3, 3, 4], 5, [0, 1, 2, 3, 4])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().removeDuplicates(test_case.nums)
    assert actual == test_case.n
    assert test_case.nums[:actual] == test_case.expected
