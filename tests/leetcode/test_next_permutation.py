from dataclasses import dataclass
import pytest

from leetcode.next_permutation import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    expected: list[int]


def case_generator():
    yield TestCase([], [])
    yield TestCase([0, 1, 2, 5, 3, 3, 0], [0, 1, 3, 0, 2, 3, 5])
    yield TestCase([3, 2, 1], [1, 2, 3])
    yield TestCase([1, 5, 1], [5, 1, 1])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    Solution().nextPermutation(test_case.nums)
    # modified in place
    assert test_case.nums == test_case.expected
