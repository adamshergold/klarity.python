from dataclasses import dataclass

import pytest

from leetcode.valid_parentheses import Solution


@dataclass(frozen=True)
class TestCase:
    s: str
    expected: bool


def case_generator():
    yield TestCase("()", True)
    yield TestCase("()[]{}", True)
    yield TestCase("(]", False)
    yield TestCase("]", False)
    yield TestCase("(", False)
    yield TestCase("(x", False)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().isValid(test_case.s)
    assert actual == test_case.expected
