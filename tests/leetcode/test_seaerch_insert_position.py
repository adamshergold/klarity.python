from dataclasses import dataclass

import pytest

from leetcode.search_insert_position import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    target: int
    expected: list[int]


def case_generator():
    yield TestCase([1, 3, 5, 6, ], 5, 2)
    yield TestCase([1, 3, 5, 6], 2, 1)
    yield TestCase([1, 3, 5, 6], 7, 4)
    yield TestCase([], 1, 0)
    yield TestCase([1], 2, 1)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().searchInsert(test_case.nums, test_case.target)
    assert actual == test_case.expected
