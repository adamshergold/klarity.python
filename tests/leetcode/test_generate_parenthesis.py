from dataclasses import dataclass

import pytest

from leetcode.generate_parenthesis import Solution


@dataclass(frozen=True)
class TestCase:
    n: int
    expected: list[str]


def case_generator():
    yield TestCase(0, [''])
    yield TestCase(1, ["()"])
    yield TestCase(3, ["((()))", "(()())", "(())()", "()(())", "()()()"])
    yield TestCase(4, ["(((())))", "((()()))", "((())())", "((()))()", "(()(()))",
                       "(()()())", "(()())()", "(())(())", "(())()()", "()((()))",
                       "()(()())", "()(())()", "()()(())", "()()()()"])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().generateParenthesis(test_case.n)
    assert actual == test_case.expected
