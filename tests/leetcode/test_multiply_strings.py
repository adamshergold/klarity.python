from dataclasses import dataclass

import pytest

from leetcode.multiply_strings import Solution


@dataclass(frozen=True)
class TestCase:
    num1: str
    num2: str
    expected: str


def case_generator():
    yield TestCase("2", "3", "6")
    yield TestCase("123", "456", "56088")
    yield TestCase("12", "3", "36")
    yield TestCase("12", "34", "408")
    yield TestCase("9103", "0", "0")


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().multiply(test_case.num1, test_case.num2)
    assert actual == test_case.expected
