from dataclasses import dataclass

import pytest

from leetcode.longest_valid_parenthesis import Solution


@dataclass(frozen=True)
class TestCase:
    s: str
    expected: int


def case_generator():
    yield TestCase("", 0)
    yield TestCase("(()", 2)
    yield TestCase(")()())", 4)
    yield TestCase("()(())", 6)
    yield TestCase("(()()", 4)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().longestValidParentheses(test_case.s)
    assert actual == test_case.expected
