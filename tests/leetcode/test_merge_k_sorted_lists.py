from dataclasses import dataclass

import pytest

from leetcode.listnode import to_list, from_list
from leetcode.merge_k_sorted_lists import Solution


@dataclass(frozen=True)
class TestCase:
    lists: list[list[int]]
    expected: list[int]


def case_generator():
    yield TestCase([], [])
    yield TestCase([[]], [])
    yield TestCase([[1, 4, 5], [1, 3, 4], [2, 6]], [1, 1, 2, 3, 4, 4, 5, 6])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    lists = list(map(lambda vs: from_list(vs), test_case.lists))
    actual = Solution().mergeKLists(lists)
    assert to_list(actual) == test_case.expected
