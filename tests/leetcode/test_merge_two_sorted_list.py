from dataclasses import dataclass

import pytest

from leetcode.listnode import to_list, from_list
from leetcode.merge_two_sorted_lists import Solution


@dataclass(frozen=True)
class TestCase:
    list1: list[int]
    list2: list[int]
    expected: list[int]


def case_generator():
    yield TestCase([1, 2, 4], [1, 3, 4], [1, 1, 2, 3, 4, 4])
    yield TestCase([], [], [])
    yield TestCase([], [0], [0])
    yield TestCase([0], [], [0])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().mergeTwoLists(from_list(test_case.list1), from_list(test_case.list2))
    assert to_list(actual) == test_case.expected
