from dataclasses import dataclass

import pytest

from leetcode.combination_sum_ii import Solution


@dataclass(frozen=True)
class TestCase:
    candidates: list[int]
    target: int
    expected: list[list[int]]


def case_generator():
    yield TestCase([10, 1, 2, 7, 6, 1, 5], 8, [[2, 6], [1, 7], [1, 2, 5], [1, 1, 6]])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().combinationSum2(test_case.candidates, test_case.target)
    assert actual == test_case.expected
