from dataclasses import dataclass
import pytest

from leetcode.rotate_image import Solution


@dataclass(frozen=True)
class TestCase:
    matrix: list[list[int]]
    expected: list[list[int]]


def case_generator():
    # yield TestCase([], [])
    yield TestCase([[1, 2, 3], [4, 5, 6], [7, 8, 9]], [[7, 4, 1], [8, 5, 2], [9, 6, 3]])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    Solution().rotate(test_case.matrix)
    assert test_case.matrix == test_case.expected
