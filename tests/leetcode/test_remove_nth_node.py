from dataclasses import dataclass

import pytest

from leetcode.listnode import to_list, from_list
from leetcode.remove_nth_node import Solution


@dataclass(frozen=True)
class TestCase:
    head: list[int]
    n: int
    expected: list[int]


def case_generator():
    yield TestCase([1, 2, 3, 4, 5], 2, [1, 2, 3, 5])
    yield TestCase([1, 2], 1, [1])
    yield TestCase([1, 2], 2, [2])
    yield TestCase([1], 1, [])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().removeNthFromEnd(from_list(test_case.head), test_case.n)
    assert to_list(actual) == test_case.expected
