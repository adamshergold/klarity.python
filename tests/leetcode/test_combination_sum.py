from dataclasses import dataclass

import pytest

from leetcode.combination_sum import Solution


@dataclass(frozen=True)
class TestCase:
    candidates: list[int]
    target: int
    expected: list[list[int]]


def case_generator():
    yield TestCase([2, 3, 6, 7], 7, [[7], [2, 2, 3]])
    yield TestCase([2], 2, [[2]])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().combinationSum(test_case.candidates, test_case.target)
    assert actual == test_case.expected
