from dataclasses import dataclass

import pytest

from leetcode.four_sum import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    target: int
    expected: list[list[int]]


def case_generator():
    yield TestCase([1, 0, -1, 0, -2, 2], 0, [[-2, -1, 1, 2], [-2, 0, 0, 2], [-1, 0, 0, 1]])
    yield TestCase([2, 2, 2, 2, 2], 8, [[2, 2, 2, 2]])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().fourSum(test_case.nums, test_case.target)
    assert actual == test_case.expected
