from dataclasses import dataclass

import pytest

from leetcode.wilcard_matching import Solution


@dataclass(frozen=True)
class TestCase:
    s: str
    p: str
    expected: bool


def case_generator():
    yield TestCase("", "", True)
    yield TestCase("aa", "a", False)
    yield TestCase("aa", "*", True)
    yield TestCase("cb", "?a", False)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().isMatch(test_case.s, test_case.p)
    assert actual == test_case.expected
