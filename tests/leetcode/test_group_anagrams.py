from dataclasses import dataclass

import pytest

from leetcode.group_anagrams import Solution


@dataclass(frozen=True)
class TestCase:
    strs: list[str]
    expected: list[list[str]]


def case_generator():
    yield TestCase(["eat", "tea", "tan", "ate", "nat", "bat"],
                   [['eat', 'tea', 'ate'], ['tan', 'nat'], ['bat']])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().groupAnagrams(test_case.strs)
    assert actual == test_case.expected
