from dataclasses import dataclass

import pytest

from leetcode.zigzag_conversion import Solution


@dataclass(frozen=True)
class TestCase:
    s: str
    rows: int
    expected: str


def case_generator():
    yield TestCase("ABC", 1, "ABC")
    yield TestCase("PAYPALISHIRING", 3, "PAHNAPLSIIGYIR")
    yield TestCase("PAYPALISHIRING", 4, "PINALSIGYAHRPI")


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().convert(test_case.s, test_case.rows)
    assert actual == test_case.expected
