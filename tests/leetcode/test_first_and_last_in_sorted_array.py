from dataclasses import dataclass

import pytest

from leetcode.first_and_last_in_sorted_array import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    target: int
    expected: list[int]


def case_generator():
    yield TestCase([5, 7, 7, 8, 8, 10], 8, [3, 4])
    yield TestCase([5, 7, 7, 8, 8, 10], 6, [-1, -1])
    yield TestCase([], 0, [-1, -1])
    yield TestCase([2, 2], 2, [0, 1])
    yield TestCase([2, 2], 3, [-1, -1])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().searchRange(test_case.nums, test_case.target)
    assert actual == test_case.expected
