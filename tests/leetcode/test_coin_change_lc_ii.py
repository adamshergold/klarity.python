from dataclasses import dataclass
import pytest

from leetcode.coin_change_ii import Solution


@dataclass(frozen=True)
class TestCase:
    coins: list[int]
    amount: int
    expected: int


def case_generator():
    yield TestCase([], 0, 1)
    yield TestCase([4], 4, 1)
    yield TestCase([4], 2, 0)
    yield TestCase([1, 2, 5], 5, 4)
    yield TestCase([2], 3, 0)
    yield TestCase([10], 10, 1)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().change(test_case.amount, test_case.coins)
    assert actual == test_case.expected
