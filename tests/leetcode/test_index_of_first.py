from dataclasses import dataclass

import pytest

from leetcode.index_of_first import Solution


@dataclass(frozen=True)
class TestCase:
    haystack: str
    needle: str
    expected: int


def case_generator():
    yield TestCase("a", "a", 0)
    yield TestCase("mississippi", "issip", 4)
    yield TestCase("sadbutsad", "", 0)
    yield TestCase("", "", -1)
    yield TestCase("sadbutsad", "sad", 0)
    yield TestCase("leetcode", "leeto", -1)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().strStr(test_case.haystack, test_case.needle)
    assert actual == test_case.expected
