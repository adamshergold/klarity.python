from dataclasses import dataclass

import pytest

from leetcode.fruits_into_baskets import Solution


@dataclass(frozen=True)
class TestCase:
    fruits: list[int]
    expected: int


def case_generator():
    yield TestCase([1, 2, 1], 3)
    yield TestCase([0, 1, 2, 2], 3)
    yield TestCase([1, 2, 3, 2, 2], 4)
    yield TestCase([3, 3, 3, 1, 2, 1, 1, 2, 3, 3, 4], 5)
    yield TestCase([1, 0, 1, 4, 1, 4, 1, 2, 3], 5)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().totalFruit(test_case.fruits)
    assert actual == test_case.expected
