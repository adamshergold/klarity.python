from dataclasses import dataclass
import pytest

from leetcode.two_sum import Solution


@dataclass(frozen=True)
class TestCase:
    arr: list[int]
    target: int
    expected: list[int]


def case_generator():
    yield TestCase([], 0, [])
    yield TestCase([2, 7, 11, 15], 9, [0, 1])
    yield TestCase([3, 2, 4], 6, [1, 2])
    yield TestCase([-1, -2, -3, -4, -5], -8, [2, 4])
    yield TestCase([2, 1, 9, 4, 4, 56, 90, 3], 8, [3, 4])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().twoSum(test_case.arr, test_case.target)
    assert actual == test_case.expected
