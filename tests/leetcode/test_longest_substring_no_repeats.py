from dataclasses import dataclass

import pytest

from leetcode.longest_substring_no_repeats import Solution


@dataclass(frozen=True)
class TestCase:
    s: str
    expected: int


def case_generator():
    yield TestCase("", 0)
    yield TestCase("a", 1)
    yield TestCase("abba", 2)
    yield TestCase("abcabcbb", 3)
    yield TestCase("bbbbb", 1)
    yield TestCase("pwwkew", 3)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().lengthOfLongestSubstring(test_case.s)
    assert actual == test_case.expected
