from dataclasses import dataclass

import pytest

from leetcode.three_sum_closest import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    target: int
    expected: int


def case_generator():
    yield TestCase([-1, 2, 1, -4], 1, 2)
    yield TestCase([0, 0, 0], 1, 0)
    yield TestCase([0], 1, 0)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().threeSumClosest(test_case.nums, test_case.target)
    assert actual == test_case.expected
