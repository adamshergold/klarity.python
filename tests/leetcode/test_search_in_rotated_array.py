from dataclasses import dataclass

import pytest

from leetcode.search_in_rotated_array import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    target: int
    expected: int


def case_generator():
    yield TestCase([4, 5, 6, 7, 0, 1, 2], 0, 4)
    yield TestCase([4, 5, 6, 7, 0, 1, 2], 3, -1)
    yield TestCase([1], 0, -1)
    yield TestCase([1, 3], 1, 0)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().search(test_case.nums, test_case.target)
    assert actual == test_case.expected
