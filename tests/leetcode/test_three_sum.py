from dataclasses import dataclass

import pytest

from leetcode.three_sum import Solution


@dataclass(frozen=True)
class TestCase:
    nums: list[int]
    expected: list[list[int]]


def case_generator():
    yield TestCase([-1, 0, 1, 2, -1, -4], [[-1, -1, 2], [-1, 0, 1]])
    yield TestCase([0, 0, 0, 0], [[0, 0, 0]])
    yield TestCase([3, 0, -2, -1, 1, 2], [[-2, -1, 3], [-2, 0, 2], [-1, 0, 1]])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution_slow(test_case):
    actual = Solution().threeSum_slow(test_case.nums)
    assert actual == test_case.expected


def test_solution(test_case):
    actual = Solution().threeSum(test_case.nums)
    assert actual == test_case.expected
