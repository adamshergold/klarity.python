from dataclasses import dataclass

import pytest

from leetcode.longest_palindrome import Solution


@dataclass(frozen=True)
class TestCase:
    s: str
    expected: str


def case_generator():
    yield TestCase("", "")
    yield TestCase("aba", "aba")
    yield TestCase("a", "a")
    yield TestCase("babad", "bab")
    yield TestCase("cbbd", "bb")
    yield TestCase("bb", "bb")
    yield TestCase("ccc", "ccc")


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().longestPalindrome(test_case.s)
    assert actual == test_case.expected
