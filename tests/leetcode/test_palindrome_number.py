from dataclasses import dataclass

import pytest

from leetcode.palindrome_number import Solution


@dataclass(frozen=True)
class TestCase:
    x: int
    expected: bool


def case_generator():
    yield TestCase(121, True)
    yield TestCase(-121, False)
    yield TestCase(10, False)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().isPalindrome(test_case.x)
    assert actual == test_case.expected
