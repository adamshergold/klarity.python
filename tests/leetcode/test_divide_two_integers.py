from dataclasses import dataclass

import pytest

from leetcode.divide_two_integers import Solution


@dataclass(frozen=True)
class TestCase:
    dividend: int
    divisor: int
    expected: int


def case_generator():
    yield TestCase(10, 3, 3)
    yield TestCase(7, -3, -2)
    yield TestCase(-1, 1, -1)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().divide(test_case.dividend, test_case.divisor)
    assert actual == test_case.expected
