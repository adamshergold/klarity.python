from dataclasses import dataclass

import pytest

from leetcode.listnode import to_list, from_list
from leetcode.swap_nodes_in_pairs import Solution


@dataclass(frozen=True)
class TestCase:
    head: list[int]
    expected: list[int]


def case_generator():
    yield TestCase([1, 2, 3, 4], [2, 1, 4, 3])
    yield TestCase([], [])
    yield TestCase([1], [1])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().swapPairs(from_list(test_case.head))
    assert to_list(actual) == test_case.expected
