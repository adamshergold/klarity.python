from dataclasses import dataclass

import pytest

from leetcode.n_queens import Solution


@dataclass(frozen=True)
class TestCase:
    n: int
    expected: list[list[str]]


def case_generator():
    yield TestCase(4, [[".Q..", "...Q", "Q...", "..Q."], ["..Q.", "Q...", "...Q", ".Q.."]])
    yield TestCase(1, [["Q"]])


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().solveNQueens(test_case.n)

    actual_to_compare = sorted(list(map(lambda vs: ''.join(vs), actual)))
    expected_to_compare = sorted(list(map(lambda vs: ''.join(vs), test_case.expected)))
    assert actual_to_compare == expected_to_compare
