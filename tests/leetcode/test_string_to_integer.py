from dataclasses import dataclass

import pytest

from leetcode.string_to_integer import Solution


@dataclass(frozen=True)
class TestCase:
    s: str
    expected: int


def case_generator():
    yield TestCase("", 0)
    yield TestCase("42", 42)
    yield TestCase("+42", 42)
    yield TestCase("-42", -42)
    yield TestCase("    -42", -42)
    yield TestCase("4193 with words", 4193)


@pytest.fixture(params=case_generator(), ids=str)
def test_case(request):
    return request.param


def test_solution(test_case):
    actual = Solution().myAtoi(test_case.s)
    assert actual == test_case.expected
