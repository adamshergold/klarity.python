class Solution:
    # given an array of coins, how many ways can we 'make' the target
    # where each coin can be used more than once
    def numberOfWays(self, coins: list[int], target: int) -> int:

        if len(coins) == 0:
            return 1 if target == 0 else 0

        # dynamic programming approach so create 2d array
        dp = [[0] * len(coins) for _ in range(target+1)]
        # dp[i][j] = number of ways to make target 'i' using coins[0..j] (inclusive)

        # the first row, where 'target == 0' is all set to 1. why? because we
        # can always make the target in exactly one way but taking none of the coins
        for j in range(len(coins)):
            dp[0][j] = 1

        for i in range(1, target + 1):
            for j in range(len(coins)):
                # we have two choices at each step: use coin j or do not use it.

                # if we do not use it then we need to see how many ways we can make 'i' using
                # coins[0..j-1] (but need to be careful when j==0)
                not_using_coin_j = dp[i][j-1] if j > 0 else 0

                # if we do use it (and the coin value must be <= i) then we see how many ways we
                # can make the smaller target of 'i-coins[j]' from coins[0..j]
                using_coin_j = dp[i-coins[j]][j] if coins[j] <= i else 0

                # add up both cases to give final answer
                dp[i][j] = not_using_coin_j + using_coin_j

        return dp[target][len(coins)-1]
