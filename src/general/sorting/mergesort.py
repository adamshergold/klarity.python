def sort(inputs):

    if len(inputs) == 0:
        return

    def impl(start, end):

        if start == end:
            return [inputs[start]]

        mid = (start + end) // 2

        # solve the sub problem
        lhs = impl(start, mid)
        rhs = impl(mid+1, end)

        result = []
        lp, rp = 0, 0
        while lp < len(lhs) and rp < len(rhs):
            if lhs[lp] <= rhs[rp]:
                result.append(lhs[lp])
                lp += 1
            else:
                result.append(rhs[rp])
                rp += 1

        for i in range(lp, len(lhs)):
            result.append(lhs[i])

        for i in range(rp, len(rhs)):
            result.append(rhs[i])

        return result

    inputs[:] = impl(0, len(inputs)-1)
