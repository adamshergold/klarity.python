"""Module containing implementation of heapsort"""


def sort(vs):
    """Sorting (in-place) function"""
    def parent(index):
        return index // 2

    def left_child(index):
        return 2 * index + 1

    def right_child(index):
        return 2 * index + 2

    def sift_down(start, end):
        root = start
        while left_child(root) <= end:
            left_child_idx = left_child(root)
            right_child_idx = right_child(root)
            swap_index = root
            if vs[root] < vs[left_child_idx]:
                swap_index = left_child_idx
            if right_child_idx <= end and vs[swap_index] < vs[right_child_idx]:
                swap_index = right_child_idx
            if swap_index == root:
                return
            swap(swap_index, root)
            root = swap_index

    def swap(x, y):
        vs[x], vs[y] = vs[y], vs[x]

    def heapify(start, end):
        root = parent(end)
        while root >= start:
            sift_down(root, end)
            root -= 1

    # step 1: make 'vs' a heap
    heapify(0, len(vs)-1)

    # step 2: loop and extract greatest element (at top of heap)
    #         and swap with end then sift down
    for i in range(len(vs)-1, 0, -1):
        swap(0, i)
        sift_down(0, i-1)
