"""Module containing implementation of quicksort"""


def sort(inputs):
    """Sorting (in place) function"""
    def swap(x, y):
        inputs[x], inputs[y] = inputs[y], inputs[x]

    def partition(start, end):
        # arbitrary / simple choice to pick pivot at end
        pivot = inputs[end]
        # invariant is that items vs in [start,m] are < pivot
        m = start - 1
        for i in range(start, end):
            if inputs[i] < pivot:
                m += 1
                swap(m, i)
        swap(end, m + 1)
        return m + 1

    def impl(start, end):
        if end <= start:
            return
        p = partition(start, end)
        impl(start, p - 1)
        impl(p + 1, end)

    impl(0, len(inputs)-1)
