class Solution:
    def fib_1(self, n: int) -> int:
        if n < 3:
            return 1
        a, b = 1, 1
        for _ in range(n - 2):
            a, b = a + b, a
        return a

    def fib_2(self, n: int) -> int:
        if n < 3:
            return 1

        def matrix_mult(a: list[list[int]], b: list[list[int]]):
            return [
                [a[0][0] * b[0][0] + a[0][1] * b[1][0], a[0][0] * b[0][1] + a[0][1] * b[1][1]],
                [a[1][0] * b[0][0] + a[1][1] * b[1][0], a[1][0] * b[0][1] + a[1][1] * b[1][1]]
            ]

        def impl(n):
            if n == 1:
                return [[1, 1], [1, 0]]
            sub_answer = impl(n // 2)
            squared = matrix_mult(sub_answer, sub_answer)
            if n % 2 == 0:
                return squared

            return matrix_mult(squared, [[1, 1], [1, 0]])

        v = impl(n)

        return v[0][1]
