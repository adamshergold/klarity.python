class Solution:
    def multiply(self, a: int, b: int) -> int:
        # let's make sure a <= b
        a, b = min(a, b), max(a, b)

        if b == 0 or a == 0:
            return 0

        def impl(n):
            if n == 1:
                return a
            sub_result = impl(n // 2)
            return sub_result + sub_result + (a if n % 2 == 1 else 0)

        return impl(b)
