# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(A):
    # An array A consisting of N different integers is given.
    # The array contains integers in the range [1..(N + 1)],
    # which means that exactly one element is missing.
    # Your goal is to find that missing element.
    #
    # use a summation trick:
    # if S_n is sum of [1..n] then S_n = n(n+1)/2
    # so we'll compare the actual sum with the 'expected' and this
    # will tell us what is missing

    n = len(A)
    actual_sum = sum(A)
    expected_sum = (n+1) * (n+2) // 2
    return expected_sum - actual_sum
