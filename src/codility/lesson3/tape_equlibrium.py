# A non-empty array A consisting of N integers is given. Array A represents numbers on a tape.
# Any integer P, such that 0 < P < N, splits this tape into two non-empty parts:
#     A[0], A[1], ..., A[P − 1] and A[P], A[P + 1], ..., A[N − 1].
# The difference between the two parts is the value of:
#     |(A[0] + A[1] + ... + A[P − 1]) − (A[P] + A[P + 1] + ... + A[N − 1])|
# In other words, it is the absolute difference between the sum of the first
# part and the sum of the second part.

def solution(A):
    lhs_sum = A[0]
    rhs_sum = sum(A[1:])
    min_abs_diff = abs(rhs_sum-lhs_sum)
    for i in range(1, len(A)-1):
        lhs_sum += A[i]
        rhs_sum -= A[i]
        min_abs_diff = min(min_abs_diff, abs(rhs_sum - lhs_sum))
    return min_abs_diff
