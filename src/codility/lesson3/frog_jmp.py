def solution(X, Y, D):
    # A small frog wants to get to the other side of the road.
    # The frog is currently located at position X and wants to get
    # to a position greater than or equal to Y. The small frog always jumps
    # a fixed distance, D.
    # Count the minimal number of jumps that the small frog must perform to reach its target.

    # simple but too slow
    # currently_at, jumps = X, 0
    # while currently_at < Y:
    #     jumps += 1
    #     currently_at += D
    # return jumps

    distance_to_jump = Y - X

    return (distance_to_jump // D) + (1 if distance_to_jump % D != 0 else 0)
