# A non-empty array A consisting of N integers is given.
# A permutation is a sequence containing each element from 1 to N once, and only once.
def solution(A):
    A.sort()
    for i in range(len(A)):
        if A[i] != i+1:
            return 0
    return 1
