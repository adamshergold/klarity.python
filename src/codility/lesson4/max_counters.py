# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(N, A):
    counters = [0] * N
    floor, max_cv = 0, 0
    for v in A:
        if v <= N:
            counters[v - 1] = max(floor, counters[v - 1]) + 1
            max_cv = max(max_cv, counters[v - 1])
        else:
            floor = max_cv
            max_cv = floor
    return [max(floor, v) for v in counters]
