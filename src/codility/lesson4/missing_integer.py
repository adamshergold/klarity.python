def solution(A):
    pvs = set()
    for v in A:
        if v >= 0:
            pvs.add(v)

    if len(pvs) == 0:
        return 1

    mv = min(pvs)

    if mv > 1:
        return 1

    candidate = mv + 1

    while True:
        if candidate not in pvs:
            return candidate
        candidate += 1
