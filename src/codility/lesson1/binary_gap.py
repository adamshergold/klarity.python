def Solution(N):
    max_gap = 0

    if N == 0:
        return 0

    # remove leading 0's
    while not N & 1:
        N = N >> 1

    # now we start
    while N > 0:
        count = 0
        while N & 1:
            N = N >> 1
        while N > 0 and not (N & 1):
            N = N >> 1
            count += 1
        max_gap = max(count, max_gap)

    return max_gap
