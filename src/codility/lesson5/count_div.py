# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(A, B, K):
    lower = A + (K - A % K if A % K != 0 else 0)
    upper = B - (B % K)
    return 1 + (upper - lower)//K
