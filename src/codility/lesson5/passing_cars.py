def solution(A):
    number_still_going_west = sum(A)
    total = 0
    for v in A:
        if v == 0:
            total += number_still_going_west
        else:
            number_still_going_west -= 1
        if total > 1000000000:
            return -1
    return total
