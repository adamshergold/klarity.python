def solution(S, P, Q):
    def to_factor(ch):
        if ch == 'A':
            return 1
        if ch == 'C':
            return 2
        if ch == 'G':
            return 3
        if ch == 'T':
            return 4
        return -1

    # calculate cumulative sums
    counts = [[0, 0, 0, 0] for _ in range(len(S))]

    for i in range(len(S)):
        factor = to_factor(S[i])
        prev = [0, 0, 0, 0] if i == 0 else counts[i-1]
        if factor == 1:
            counts[i] = [prev[0]+1, prev[1], prev[2], prev[3]]
        elif factor == 2:
            counts[i] = [prev[0], prev[1]+1, prev[2], prev[3]]
        elif factor == 3:
            counts[i] = [prev[0], prev[1], prev[2]+1, prev[3]]
        else:
            counts[i] = [prev[0], prev[1], prev[2], prev[3]+1]

    # build the result
    result = [0] * len(P)

    def min_between(pi, qi):

        counts_start = counts[pi-1] if pi > 0 else [0, 0, 0, 0]
        counts_end = counts[qi]

        if pi == qi:
            return to_factor(S[pi])

        if counts_end[0] > counts_start[0]:
            return 1
        elif counts_end[1] > counts_start[1]:
            return 2
        elif counts_end[2] > counts_start[2]:
            return 3
        else:
            return 4

    for i in range(len(P)):
        result[i] = min_between(P[i], Q[i])

    return result
