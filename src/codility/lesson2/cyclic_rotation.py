# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

def solution(A, K):

    if len(A) == 0:
        return []

    result = [v for v in A]
    n = len(result)

    def rotate():
        result[0], result[1:] = result[n-1], result[0:n-1]

    for i in range(K):
        rotate()

    return result
