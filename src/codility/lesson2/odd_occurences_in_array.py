from collections import Counter


def solution(A):
    counted = Counter(A)
    for k in counted.keys():
        if counted[k] % 2 == 1:
            return k
