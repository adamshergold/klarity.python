class Solution:
    def isMatch(self, s: str, p: str) -> bool:

        # dp[i][j] indicates if first i chars of s matches first j of pattern

        len_s, len_p = len(s), len(p)

        # initialise our dp array
        dp = [[False] * (len_p + 1) for _ in range(len_s + 1)]

        # dp[0][0] = True (empty string, empty pattern)
        dp[0][0] = True

        # empty string can match a pattern that allows it i.e. *
        for j in range(1, len_p + 1):
            if p[j - 1] == '*':
                dp[0][j] = dp[0][j - 1]

        for i in range(1, len_s + 1):
            for j in range(1, len_p + 1):

                # cases: if the pattern ending here is ? then the answer is just
                # that wound back one in the text and pattern
                if p[j - 1] == '?':
                    dp[i][j] = dp[i - 1][j - 1]
                elif p[j - 1] == '*':
                    # if the pattern ends here is '*' then its either:
                    # the same text but one less on the pattern (i.e. * means 0 times)
                    # or its the same pattern but one less in the text
                    dp[i][j] = dp[i][j - 1] or dp[i - 1][j]
                else:
                    # or finally the most specific match i.e. check pattern and text agree
                    dp[i][j] = p[j - 1] == s[i - 1] and dp[i - 1][j - 1]

        return dp[len_s][len_p]
