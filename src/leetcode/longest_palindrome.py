class Solution:
    def longestPalindrome(self, s: str) -> str:

        def longest_here(i):
            sl, sr = i, i
            while sl > 0 and s[sl - 1] == s[i]:
                sl -= 1
            while sr < len(s) - 1 and s[sr + 1] == s[i]:
                sr += 1
            while sl > 0 and sr < len(s) - 1 and s[sl - 1] == s[sr + 1]:
                sl -= 1
                sr += 1
            return s[sl:sr + 1]

        longest = ""
        for i in range(len(s)):
            candidate = longest_here(i)
            if len(candidate) > len(longest):
                longest = candidate

        return longest
