class Solution:
    def isMatch(self, s: str, p: str) -> bool:

        # dp[i][j] indicates if first i chars of s matches first j of pattern

        len_s, len_p = len(s), len(p)

        # initialise our dp array
        dp = [[False] * (len_p + 1) for _ in range(len_s + 1)]

        # dp[0][0] = True (empty string, empty pattern)
        dp[0][0] = True

        # another base-case: an empty string matches any pattern that is of the form 'x*'
        # e.g. a*b* or a* or .*a*b* ... but see the pattern?  if the pattern ends in x* then
        # we look back till either the pattern is empty or its not y*
        for j in range(2, len_p + 1):
            dp[0][j] = dp[0][j - 2] if p[j - 1] == '*' else False

        for i in range(1, len_s + 1):
            for j in range(1, len_p + 1):

                # first case ... the pattern ends with *
                if j > 1 and p[j - 1] == '*':
                    # if the pattern is 'x*' then there are two choices:
                    # either x* is a 'null' match i.e. x repeated 0 times
                    # in which case the answer is the answer less the x* of the
                    # pattern
                    dp[i][j] = dp[i][j - 2]
                    # the other case is the pattern does match i.e. 'x*' and the
                    # last char of text is either 'x' or the pattern is '.'
                    # in this case we keep the patten as-is and drop back one in the
                    # text ... why? well suppose the text is 'xxxx' and our pattern is 'x*'
                    # then we need to apply 'x*' to 'xxx' and then to 'xx' etc.
                    if p[j - 2] == '.' or p[j - 2] == s[i - 1]:
                        dp[i][j] = dp[i][j] or dp[i - 1][j]
                elif p[j - 1] == '.':
                    # easy ..
                    dp[i][j] = dp[i - 1][j - 1]
                else:
                    # easy too
                    dp[i][j] = dp[i - 1][j - 1] and s[i - 1] == p[j - 1]

        return dp[len_s][len_p]
