class Solution:
    def myAtoi(self, s: str) -> int:
        maxv, sign, i = pow(2, 31), 1, 0
        while i < len(s) and s[i] == ' ':
            i += 1
        if i == len(s):
            return 0
        if s[i] == '-':
            i += 1
            sign = -1
        elif s[i] == '+':
            i += 1
        result = 0
        while i < len(s) and s[i].isdigit():
            result = result * 10 + (ord(s[i]) - ord('0'))
            i += 1
        if sign > 0:
            return min(result, maxv - 1)

        return -1 * min(result, maxv)
