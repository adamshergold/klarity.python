from typing import List


class Solution:
    def rotate(self, matrix: List[List[int]]) -> None:

        if len(matrix) == 0:
            return

        n = len(matrix[0])

        iterations = (n + 1) // 2

        for i in range(iterations):

            x, y, w = i, i, (n - 2 * i)

            if w > 1:
                for p in range(w-1):
                    keep = matrix[y][x + p]
                    matrix[y][x + p] = matrix[y + w - p - 1][x]
                    matrix[y + w - 1 - p][x] = matrix[y + w - 1][x + w - 1 - p]
                    matrix[y + w - 1][x + w - 1 - p] = matrix[y + p][x + w - 1]
                    matrix[y + p][x + w - 1] = keep

        return
