from typing import List


class Solution:

    def solveSudoku(self, board: List[List[str]]) -> None:

        n_rows, n_cols = len(board), len(board[0])

        def subboxes_ok():

            def check_subbox(tl_row, tl_col):
                seen = set()
                for i in range(3):
                    for j in range(3):
                        v = board[tl_row + i][tl_col + j]
                        if v != '.' and v in seen:
                            return False
                        else:
                            seen.add(v)
                return True

            # check the sub-boxes
            for row in range(3):
                for col in range(3):
                    if not check_subbox(row * 3, col * 3):
                        return False

            return True

        def next_position():
            for r in range(n_rows):
                for c in range(n_cols):
                    if board[r][c] == '.':
                        return r, c
            return None, None

        def candidates(at_row, at_col):
            seen = set()
            for c in range(n_cols):
                seen.add(board[at_row][c])
            for r in range(n_rows):
                seen.add(board[r][at_col])
            for v in ['1', '2', '3', '4', '5', '6', '7', '8', '9']:
                if v not in seen:
                    yield v

        def solve():

            next_r, next_c = next_position()

            if next_r is None:
                return True

            for v in candidates(next_r, next_c):
                board[next_r][next_c] = v
                if subboxes_ok() and solve():
                    return True
                else:
                    board[next_r][next_c] = '.'

            return False

        solve()
