from typing import Optional


class ListNode:
    def __init__(self, val=0, nxt=None):
        self.val = val
        self.next = nxt


class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:

        hd, tl, carry, e1, e2 = None, None, 0, l1, l2

        while e1 is not None or e2 is not None:

            v = carry + (e1.val if e1 is not None else 0) + (e2.val if e2 is not None else 0)

            if v > 9:
                carry, v = 1, v - 10
            else:
                carry = 0

            if hd is None:
                hd = ListNode(v)
                tl = hd
            else:
                tl.next = ListNode(v)
                tl = tl.next

            e1, e2 = e1.next if e1 is not None else None, e2.next if e2 is not None else None

        # need to check for any final carry
        if carry > 0:
            tl.next = ListNode(carry)
        return hd
