from typing import List


class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:

        def impl(vs, t):
            if len(vs) == 0:
                return []

            result, ws, attempts = [], [], t // vs[0]

            for i in range(0, attempts+1):

                ws_sum = sum(ws)

                if ws_sum == t:
                    result.append([v for v in ws])
                elif t > ws_sum:
                    for child in impl(vs[1:], t - ws_sum):
                        result.append([v for v in ws + child])

                ws.append(vs[0])

            return result

        return impl(candidates, target)
