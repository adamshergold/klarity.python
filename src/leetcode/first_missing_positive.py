from typing import List


class Solution:
    def firstMissingPositive(self, nums: List[int]) -> int:

        m, pv = -1, 1

        for i in range(len(nums)):
            if nums[i] < pv:
                m += 1
                nums[m], nums[i] = nums[i], nums[m]

        positive_start_index = m + 1

        number_of_positive_values = len(nums) - positive_start_index

        if number_of_positive_values == 0:
            return 1

        for i in range(positive_start_index, len(nums)):
            if nums[i] <= number_of_positive_values:
                index_to_flip = positive_start_index + abs(nums[i]) - 1
                if index_to_flip < len(nums):
                    nums[index_to_flip] = -abs(nums[index_to_flip])

        current = positive_start_index

        while current < len(nums) and nums[current] < 0:
            current += 1

        logical_index_at_current = current - positive_start_index + 1

        return logical_index_at_current
