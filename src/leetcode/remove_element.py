from typing import List


class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        next, i = -1, 0
        while i < len(nums):
            if nums[i] != val:
                next += 1
                nums[next] = nums[i]
            i += 1
        return next + 1
