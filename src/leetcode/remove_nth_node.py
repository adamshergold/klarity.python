from collections import deque
from typing import Optional
from leetcode.listnode import ListNode


class Solution:
    def removeNthFromEnd(self, head: Optional[ListNode], n: int) -> Optional[ListNode]:
        nodes, ptr = deque(), head
        while ptr is not None:
            nodes.appendleft(ptr)
            ptr = ptr.next
        for _ in range(n - 1):
            nodes.popleft()
        to_remove = nodes.popleft()
        if nodes:
            prior = nodes.popleft()
            prior.next = to_remove.next
        else:
            head = to_remove.next
        return head
