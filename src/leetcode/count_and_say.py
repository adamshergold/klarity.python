class Solution:
    def countAndSay(self, n: int) -> str:

        def say(v: str):
            start, i, count, result = 0, 1, 1, []
            while i < len(v):
                if v[i] == v[start]:
                    count += 1
                else:
                    result.append(f'{count}{v[start]}')
                    start = i
                    count = 1
                i += 1
            result.append(f'{count}{v[start]}')
            return ''.join(result)

        def impl(i):

            if i == 1:
                return "1"

            i_minus_1 = impl(i - 1)

            return say(i_minus_1)

        return impl(n)
