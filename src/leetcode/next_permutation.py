from typing import List


class Solution:
    def nextPermutation(self, nums: List[int]) -> None:

        def swap(x, y):
            nums[x], nums[y] = nums[y], nums[x]

        def reverse(from_index, to_index):
            i, j = from_index, to_index
            while i < j:
                swap(i, j)
                i += 1
                j -= 1

        # deal with trivial cases e.g. [] and [1]
        if len(nums) < 2:
            return

            # step 1: work from the right and find the point where the sequence
        # stops being non-decreasing. The point where it 'drops' is the pivot
        i = len(nums) - 1
        while i > 0 and nums[i - 1] >= nums[i]:
            i -= 1

        pivot_index = i - 1

        # step 2: from the pivot index to the end find the
        # right-most item that is less than the pivot
        #
        # note: if the pivot_index is -1 (which it could
        # be) then it means the list is strictly non-increasing
        # so we don't do anything except reverse it
        if pivot_index >= 0:
            swap_index = len(nums) - 1

            while swap_index > 0 and nums[pivot_index] >= nums[swap_index]:
                swap_index -= 1

            swap(swap_index, pivot_index)

        # then we swap
        reverse(pivot_index + 1, len(nums) - 1)

        return None
