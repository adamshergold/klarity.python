from typing import List


class Solution:
    def totalFruit(self, fruits: List[int]) -> int:

        start, end, basket, max_fruit = 0, 0, {}, 0

        for end in range(len(fruits)):

            current_fruit = fruits[end]

            basket[current_fruit] = basket.get(current_fruit, 0) + 1

            while len(basket) > 2:
                basket[fruits[start]] -= 1
                if basket[fruits[start]] == 0:
                    del basket[fruits[start]]
                start += 1

            max_fruit = max(max_fruit, sum(basket.values()))

        return max_fruit
