from typing import List


class Solution:
    def change(self, amount: int, coins: List[int]) -> int:
        if len(coins) == 0:
            return 1 if amount == 0 else 0

        # dynamic programming approach
        # dp[i][j] = number of ways to make 'i' from coins[0..j] (inclusive)
        # i will range from 0 to amount so lets setup:
        dp = [[0] * len(coins) for _ in range(amount + 1)]
        # some initial conditions: can always make 0 from any coins in just
        # 1 way i.e. none!
        for j in range(len(coins)):
            dp[0][j] = 1

        for i in range(1, amount + 1):
            for j in range(len(coins)):
                # two choice at each point: either we use the coin or we don't
                number_of_ways_using_the_coin = dp[i - coins[j]][j] if coins[j] <= i else 0
                number_of_ways_without_using_the_coin = dp[i][j - 1] if j > 0 else 0
                dp[i][j] = number_of_ways_using_the_coin + number_of_ways_without_using_the_coin

        return dp[amount][len(coins) - 1]
