from typing import List


class Solution:
    def maxArea(self, height: List[int]) -> int:
        if len(height) == 0:
            return 0

        # two points: one on the left side and one of the right
        # we'll keep moving these towards each other and keep
        # track of max as we go along

        max_seen, l_ptr, r_ptr = 0, 0, len(height) - 1
        while l_ptr < r_ptr:
            # the maximum with current values of l_ptr and r_ptr is just
            # the distance between them times the lowest height
            # (since we can't overflow)
            max_here = (r_ptr - l_ptr) * min(height[l_ptr], height[r_ptr])
            # if this local max is bigger than global max then we update
            max_seen = max(max_here, max_seen)
            # how to move towards terminating? we must move one of the two
            # pointers so they are closer together.  No point moving the
            # pointer which is the greatest height of the two since that
            # can't possibly increase the local max (since it'll be
            # 'min-d' by the lower one) ... so move the lowest pointer
            if height[l_ptr] < height[r_ptr]:
                l_ptr += 1
            else:
                r_ptr -= 1
        return max_seen
