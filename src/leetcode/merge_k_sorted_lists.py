from typing import List, Optional
from leetcode.listnode import ListNode


class Solution:
    def mergeKLists(self, lists: List[Optional[ListNode]]) -> Optional[ListNode]:
        def merge_two(l1: ListNode, l2: ListNode):

            if l1 is None:
                return l2

            if l2 is None:
                return l1

            hd, ptr, p1, p2 = None, None, l1, l2

            def update(hd, ptr, n):
                if hd is None:
                    return n, n
                else:
                    ptr.next = n
                    return hd, n

            while p1 is not None or p2 is not None:

                if p1 is not None and p2 is not None:
                    if p1.val < p2.val:
                        hd, ptr = update(hd, ptr, p1)
                        ptr = p1
                        p1 = p1.next
                    else:
                        hd, ptr = update(hd, ptr, p2)
                        ptr = p2
                        p2 = p2.next
                elif p1 is not None:
                    hd, ptr = update(hd, ptr, p1)
                    ptr = p1
                    p1 = p1.next

                else:
                    hd, ptr = update(hd, ptr, p2)
                    p2 = p2.next

            return hd

        def merge(vs: List[Optional[ListNode]]) -> Optional[ListNode]:

            if len(vs) == 0:
                return None

            if len(vs) == 1:
                return vs[0]

            if len(vs) == 2:
                return merge_two(vs[0], vs[1])

            mid = len(vs) // 2

            lhs, rhs = merge(vs[0:mid]), merge(vs[mid:])

            return merge_two(lhs, rhs)

        return merge(lists)
