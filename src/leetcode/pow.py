class Solution:
    def myPow(self, x: float, n: int) -> float:

        def impl(_ix, _in):
            if _in == 0:
                return 1
            v = impl(_ix, _in // 2)
            if _in % 2 == 0:
                return v * v
            else:
                return _ix * v * v

        return impl(1 / x if n < 0 else x, abs(n))
