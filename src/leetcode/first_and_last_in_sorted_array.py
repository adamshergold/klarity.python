from typing import List


class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:

        if len(nums) == 0:
            return [-1, -1]

        i, j = 0, len(nums)
        while i < j:
            mid = (i + j) // 2
            if nums[mid] < target:
                i = mid + 1
            else:
                j = mid

        start = i if i < len(nums) and nums[i] == target else -1

        if start == -1:
            return [-1, -1]

        i, j = 0, len(nums)
        while i < j:
            mid = (i + j) // 2
            if nums[mid] <= target:
                i = mid + 1
            else:
                j = mid

        end = i - 1 if i <= len(nums) and nums[i - 1] == target else -1

        return [start, end]
