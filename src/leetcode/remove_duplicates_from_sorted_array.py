from typing import List


class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        next, i = 0, 1
        while i < len(nums):
            if nums[next] != nums[i]:
                nums[next + 1] = nums[i]
                next += 1
            i += 1
        return next + 1
