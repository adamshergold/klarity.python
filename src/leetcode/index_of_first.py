class Solution:
    def strStr(self, haystack: str, needle: str) -> int:

        if len(needle) == 0:
            return -1 if len(haystack) == 0 else 0

        i = 0

        while i <= len(haystack) - len(needle):
            j, match = 0, True
            while match and j < len(needle):
                match = match and haystack[i + j] == needle[j]
                j += 1
            if match:
                return i
            i += 1

        return -1
