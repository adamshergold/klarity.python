class Solution:
    def divide(self, dividend: int, divisor: int) -> int:

        ub, positive = pow(2, 31), (dividend > 0 and divisor > 0) or (dividend < 0 and divisor < 0)

        def check(q):
            if positive:
                return ub - 1 if q > ub - 1 else q
            else:
                return -ub if q > ub else -q

        if abs(divisor) == 1:
            return check(abs(dividend))

        n, current, decrement = 0, abs(dividend), abs(divisor)

        while current >= decrement and current >= 0:

            current_n, current_decrement = 1, decrement
            while (current_decrement << 1) < current:
                current_decrement = current_decrement << 1
                current_n *= 2

            current -= current_decrement
            n += current_n

        return check(n)
