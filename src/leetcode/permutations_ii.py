from typing import List


class Solution:
    def permuteUnique(self, nums: List[int]) -> List[List[int]]:

        def impl(vs):
            if len(vs) == 1:
                return [[vs[0]]]
            result = []
            for i in range(len(vs)):
                candidate = vs[i]
                remainder = vs[0:i] + vs[i + 1:]
                sub_problem = impl(remainder)
                to_add = list(map(lambda v: [candidate] + v, sub_problem))
                result += to_add
            return result

        distinct = set()

        for v in impl(nums):
            distinct.add(tuple(v))

        return [list(e) for e in distinct]
