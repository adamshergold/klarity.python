from typing import List


class Solution:
    def generateParenthesis(self, n: int) -> List[str]:

        def impl(prefix, open_count, closed_count):

            if open_count == n and closed_count == n:
                return [prefix]

            result = []

            if open_count < n:
                result += impl(prefix + '(', open_count + 1, closed_count)

            if closed_count < open_count:
                result += impl(prefix + ')', open_count, closed_count + 1)

            return result

        return sorted(impl('', 0, 0))
