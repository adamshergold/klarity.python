from typing import List


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        index_of = {}
        for i in range(len(nums)):
            index_of[nums[i]] = i
        for i in range(len(nums)):
            match = index_of.get(target - nums[i], -1)
            if match != -1 and match != i:
                return [i, match]
        return []
