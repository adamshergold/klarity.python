from typing import List


class Solution:
    def sortedSquares(self, nums: List[int]) -> List[int]:
        start_index = 0
        while start_index < len(nums) and nums[start_index] < 0:
            start_index += 1
        positive_index, negative_index = start_index, start_index - 1
        result = []
        while positive_index < len(nums) or negative_index >= 0:
            while positive_index < len(nums) and (
                    negative_index < 0
                    or nums[positive_index] <= abs(nums[negative_index])):
                result.append(pow(nums[positive_index], 2))
                positive_index += 1
            while negative_index >= 0 and (
                    positive_index >= len(nums) or
                    abs(nums[negative_index]) <= abs(nums[positive_index])):
                result.append(pow(nums[negative_index], 2))
                negative_index -= 1
        return result
