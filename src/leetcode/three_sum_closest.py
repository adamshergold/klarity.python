from typing import List


class Solution:
    def threeSumClosest(self, nums: List[int], target: int) -> int:

        if len(nums) < 3:
            return 0
        nums_sorted, n = sorted(nums), len(nums)
        closest = nums_sorted[0] + nums_sorted[1] + nums_sorted[2]
        for i in range(1, n - 1):
            l_ptr, r_ptr = 0, n - 1
            while l_ptr < i < r_ptr:
                sum_here = nums_sorted[l_ptr] + nums_sorted[i] + nums_sorted[r_ptr]
                if abs(sum_here - target) < abs(closest - target):
                    closest = sum_here
                if sum_here < target:
                    l_ptr += 1
                else:
                    r_ptr -= 1
        return closest
