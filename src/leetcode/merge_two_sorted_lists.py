from typing import Optional

from leetcode.listnode import ListNode


class Solution:

    def mergeTwoLists(self, list1: Optional[ListNode], list2: Optional[ListNode]) \
            -> Optional[ListNode]:

        hd, ptr, p1, p2 = None, None, list1, list2

        def update(hd, ptr, n):
            if hd is None:
                return n, n
            else:
                ptr.next = n
                return hd, n

        while p1 is not None or p2 is not None:

            if p1 is not None and p2 is not None:
                if p1.val < p2.val:
                    hd, ptr = update(hd, ptr, p1)
                    ptr = p1
                    p1 = p1.next
                else:
                    hd, ptr = update(hd, ptr, p2)
                    ptr = p2
                    p2 = p2.next
            elif p1 is not None:
                hd, ptr = update(hd, ptr, p1)
                ptr = p1
                p1 = p1.next

            else:
                hd, ptr = update(hd, ptr, p2)
                p2 = p2.next

        return hd
