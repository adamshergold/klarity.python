class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        longest, start = 0, 0
        chars = {}
        for i in range(len(s)):
            if s[i] not in chars:
                chars[s[i]] = i
            else:
                start = chars[s[i]] + 1
                for key in [c for c in chars if chars[c] < start]:
                    chars.pop(key)
                chars[s[i]] = i
            longest = max(longest, i - start + 1)

        return longest
