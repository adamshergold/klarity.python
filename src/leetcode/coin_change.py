from typing import List


class Solution:
    def coinChange_0(self, coins: List[int], amount: int) -> int:

        def my_min(a, b):
            if a is None:
                return b
            if b is None:
                return a
            return min(a, b)

        def impl(k):
            if k == 0:
                return 0
            result = None
            for coin in coins:
                if coin <= k:
                    sub_result = impl(k - coin)
                    if sub_result is not None:
                        result = my_min(result, 1 + sub_result)
            return result

        answer = impl(amount)
        return answer if answer is not None else -1

    def coinChange_1(self, coins: List[int], amount: int) -> int:
        # setup dynamic programming table
        # dp[i] = min coins required to make 'i' (None == not possible)
        # initial condition is dp[0] = 0 i.e. 0 coins required to make 0
        dp = [None if i > 0 else 0 for i in range(amount + 1)]

        def my_min(a, b):
            if a is None:
                return b
            if b is None:
                return a
            return min(a, b)

        for i in range(1, amount + 1):
            # loop through each coin and, if its less than or equal
            # the current target then we see if we can make the target
            # less this coin, if we can then we can make current target
            # as 1 plus this one
            mv = None
            for j in range(len(coins)):
                if coins[j] <= i and dp[i - coins[j]] is not None:
                    mv = my_min(mv, 1 + dp[i - coins[j]])
            dp[i] = mv

        answer = dp[amount]
        return -1 if answer is None else answer
