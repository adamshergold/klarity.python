from typing import List


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        if len(strs) == 0:
            return ""
        i, n = 0, len(strs)
        while True:
            for j in range(n):
                if i >= len(strs[j]):
                    return strs[0][:i]
                if strs[j][i] != strs[0][i]:
                    return strs[0][:i]
            i += 1
