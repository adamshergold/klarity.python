class Solution:
    def isMatch(self, s: str, p: str) -> bool:

        memoised_results = {}

        def match(s_idx, p_idx):

            # need to speed things up with memoization!
            memoised_key = f'{s_idx}:{p_idx}'
            if memoised_key in memoised_results:
                return memoised_results.get(memoised_key)

            # base case: if the pattern is empty then it's a match only if
            # the string is also empty
            if p_idx == len(p):
                return s_idx == len(s)
            # so what do we do now?  3 cases really:
            # p[p_idx] is either '*', '.' or a char
            # if it's a . or a char we can do something simple which is just
            # check for a match with s[s_idx] and then move the pointers along
            # but for * it's a bit harder (note also that a * must appear after another char)

            # first things first: is there a simple match 'right here'
            # i.e. either the pattern is a '.' (and so we don't care about s[s_idx]
            # or the pattern and s must match)
            is_simple_match_here = s_idx < len(s) and (p[p_idx] == '.' or s[s_idx] == p[p_idx])

            if p_idx < len(p) - 1 and p[p_idx + 1] == '*':
                # so two cases to think about ... if the pattern is 'X*' then
                # if s[s_idx] is X then that's okay but X might be repeated so
                # we move along the s_idx pointer and make recursive call
                # if not, then we might still have a match (since X* means 0 or more)
                # if we keep S where it is and move the pattern along two steps
                result = match(s_idx, p_idx + 2) \
                         or (is_simple_match_here and match(s_idx + 1, p_idx))
            else:
                # easier case to deal with ... if we have a . or a char match
                # at the two ptr values we are good so we move along to the next
                # s char and p char i.e. recursive call
                result = is_simple_match_here and match(s_idx + 1, p_idx + 1)

            memoised_results[memoised_key] = result

            return result

        return match(0, 0)
