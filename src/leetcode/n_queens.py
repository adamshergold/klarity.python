from typing import List


class Solution:
    def solveNQueens(self, n: int) -> List[List[str]]:

        results = []

        board = [[False] * n for _ in range(n)]

        def save_board():
            return [''.join(map(lambda v: "Q" if v else ".", r)) for r in board]

        def safe(row, col):

            for i in range(0, col):

                if board[row][i]:
                    return False

                distance = col - i

                up_diag_row = row - distance

                if up_diag_row >= 0 and board[up_diag_row][i]:
                    return False

                dn_diag_row = row + distance

                if dn_diag_row < n and board[dn_diag_row][i]:
                    return False

            return True

        def solve(c):

            if c == n:
                results.append(save_board())

            for r in range(n):
                if safe(r, c):
                    board[r][c] = True
                    solve(c + 1)
                    board[r][c] = False

        solve(0)

        return results
