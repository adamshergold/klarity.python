from typing import List


class Solution:
    def search(self, nums: List[int], target: int) -> int:
        # find the pivot point first
        i, j = 0, len(nums) - 1
        while i < j:
            mid = (i + j) // 2
            if nums[mid] > nums[j]:
                i = mid + 1
            else:
                j = mid

        shifted_right_by = i
        l_i, l_j = 0, len(nums) - 1

        def logical_to_physical_index(k):
            return (k + shifted_right_by) % len(nums)

        while l_i < l_j:
            l_mid = (l_i + l_j) // 2
            v_at_l_mid = nums[logical_to_physical_index(l_mid)]
            if v_at_l_mid < target:
                l_i = l_mid + 1
            else:
                l_j = l_mid

        return logical_to_physical_index(l_i) \
            if nums[logical_to_physical_index(l_i)] == target else -1
