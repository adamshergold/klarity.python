from typing import List


class Solution:
    def jump(self, nums: List[int]) -> int:

        if len(nums) == 1:
            return 0

        max_reachable, jumps_so_far, current, end, n = 0, 0, 0, 0, len(nums)

        while current < n:

            max_reachable = max(max_reachable, current + nums[current])

            if max_reachable >= n-1:
                return jumps_so_far+1

            if current == end:
                jumps_so_far += 1
                end = max_reachable

            current += 1

        return -1
