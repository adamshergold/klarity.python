from functools import reduce
from typing import List


class ListNode:
    def __init__(self, val=0, nxt=None):
        self.val = val
        self.next = nxt


def from_list(vs: List):
    return reduce(lambda acc, v: ListNode(v, acc), reversed(vs), None)


def to_list(hd):
    def loop(n: ListNode):
        if n is not None:
            yield n.val
            yield from loop(n.next)

    return list(loop(hd))
