from typing import List


class Solution:
    def fourSum(self, nums: List[int], target: int) -> List[List[int]]:
        sorted_nums, n, vs = sorted(nums), len(nums), set()
        for i in range(n):
            for j in range(1, n - 1):
                if j == i:
                    continue
                l_ptr, r_ptr = 0, n - 1
                while l_ptr < j < r_ptr:
                    if l_ptr == i:
                        l_ptr += 1
                    elif r_ptr == i:
                        r_ptr -= 1
                    else:
                        sum_here = sorted_nums[i] + sorted_nums[l_ptr] + \
                                   sorted_nums[j] + sorted_nums[r_ptr]
                        if sum_here == target:
                            if i < l_ptr:
                                vs.add((sorted_nums[i], sorted_nums[l_ptr],
                                        sorted_nums[j], sorted_nums[r_ptr]))
                            elif i < j:
                                vs.add((sorted_nums[l_ptr], sorted_nums[i],
                                        sorted_nums[j], sorted_nums[r_ptr]))
                            elif i < r_ptr:
                                vs.add((sorted_nums[l_ptr], sorted_nums[j],
                                        sorted_nums[i], sorted_nums[r_ptr]))
                            else:
                                vs.add((sorted_nums[l_ptr], sorted_nums[j],
                                        sorted_nums[r_ptr], sorted_nums[i]))
                            l_ptr += 1
                        elif sum_here < target:
                            l_ptr += 1
                        else:
                            r_ptr -= 1
        return list(map(lambda t: [t[0], t[1], t[2], t[3]], sorted(vs)))
