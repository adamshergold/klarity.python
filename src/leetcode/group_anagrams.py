from typing import List


class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        d = dict()
        for word in strs:
            key = ''.join(sorted(word))
            if d.get(key) is not None:
                d[key].append(word)
            else:
                d[key] = [word]
        result = []
        for key in d.keys():
            result.append(d[key])
        return result
