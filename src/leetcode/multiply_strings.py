import functools
from collections import deque


class Solution:
    def multiply(self, num1: str, num2: str) -> str:

        top, bottom = (num1, num2) if len(num1) >= len(num2) else (num2, num1)

        def c2i(c):
            return ord(c) - ord('0')

        def i2c(i):
            return chr(ord('0') + i)

        def add(s1, s2):
            add_carry, add_result = 0, deque()
            for i in range(0, max(len(s1), len(s2))):
                s1i = len(s1) - i - 1
                s2i = len(s2) - i - 1
                v1 = c2i(s1[s1i]) if s1i >= 0 else 0
                v2 = c2i(s2[s2i]) if s2i >= 0 else 0
                add_v = v1 + v2 + add_carry
                add_result.appendleft(i2c(add_v % 10))
                add_carry = add_v // 10
            if add_carry > 0:
                add_result.appendleft(i2c(add_carry))
            return list(add_result)

        vs = []

        for i in range(len(bottom) - 1, -1, -1):

            result, b, carry = deque(), c2i(bottom[i]), 0

            for j in range(len(top) - 1, -1, -1):
                v = c2i(top[j]) * b + carry
                carry = v // 10
                result.appendleft(i2c(v % 10))

            if carry > 0:
                result.appendleft(i2c(carry))

            for j in range(len(bottom) - i - 1):
                result.append('0')

            vs.append(list(result))

        result = functools.reduce(lambda acc, sv: add(acc, sv), vs, [])
        start = 0
        while start < len(result)-1 and result[start] == '0':
            start += 1

        return ''.join(result[start:])
