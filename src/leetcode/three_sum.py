from typing import List


class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        sorted_nums, n, vs = sorted(nums), len(nums), set()
        for i in range(1, n - 1):
            l_ptr, r_ptr = 0, n - 1
            while l_ptr < i < r_ptr:
                sum_here = sorted_nums[l_ptr] + sorted_nums[i] + sorted_nums[r_ptr]
                if sum_here == 0:
                    vs.add((sorted_nums[l_ptr], sorted_nums[i], sorted_nums[r_ptr]))
                    l_ptr += 1
                elif sum_here < 0:
                    l_ptr += 1
                else:
                    r_ptr -= 1
        return list(map(lambda t: [t[0], t[1], t[2]], sorted(vs)))

    def threeSum_slow(self, nums: List[int]) -> List[List[int]]:
        vs = set()
        for i in range(len(nums)):
            for j in range(i + 1, len(nums)):
                for k in range(j + 1, len(nums)):
                    if i != j and j != k and (nums[i] + nums[j] + nums[k]) == 0:
                        xs = sorted([nums[i], nums[j], nums[k]])
                        vs.add((xs[0], xs[1], xs[2]))
        return list(map(lambda t: [t[0], t[1], t[2]], sorted(vs)))
