class Solution:
    def reverse(self, x: int) -> int:
        lb, ub = pow(2, 31), pow(2, 31) - 1
        result = 0
        abs_x = abs(x)
        sign = 1 if x >= 0 else -1
        while abs_x > 0:
            result = result * 10 + (abs_x % 10)
            if sign < 0 and result > lb:
                return 0
            if sign > 0 and result > ub:
                return 0
            abs_x = abs_x // 10
        return sign * result
