class Solution:
    def intToRoman(self, num: int) -> str:
        result = []
        while num >= 1000:
            result.append('M')
            num -= 1000
        while num >= 900:
            result.append('CM')
            num -= 900
        while num >= 500:
            result.append('D')
            result.append('C' * ((num - 500) // 100))
            num = num % 100
        while num >= 400:
            result.append('CD')
            num -= 400
        while num >= 100:
            result.append('C'*(1+(num - 100) // 100))
            num = num % 100
        while num >= 90:
            result.append('XC')
            num -= 90
        while num >= 50:
            result.append('L')
            result.append('X' * ((num - 50) // 10))
            num = num % 10
        while num >= 40:
            result.append('XL')
            num -= 40
        while num >= 10:
            result.append('X' * (1+(num - 10) // 10))
            num = num % 10
        quick = {
            9: 'IX',
            8: 'VIII',
            7: 'VII',
            6: 'VI',
            5: 'V',
            4: 'IV',
            3: 'III',
            2: 'II',
            1: 'I',
        }
        if num > 0:
            result.append(quick.get(num))
        return ''.join(result)
