class Solution:
    def isPalindrome(self, x: int) -> bool:
        # negative can't be due to '-' sign
        if x < 0:
            return False
        x_reversed, x_copy = 0, x
        while x_copy > 0:
            x_reversed = x_reversed * 10 + (x_copy % 10)
            x_copy = x_copy // 10

        if x_reversed == x:
            return True

        return False
