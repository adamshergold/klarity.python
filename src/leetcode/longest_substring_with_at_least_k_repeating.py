from collections import Counter


class Solution:
    def longestSubstring(self, s: str, k: int) -> int:

        if len(s) == 0:
            return 0

        i, count = 0, Counter(s)

        while i < len(s) and count[s[i]] >= k:
            i += 1

        if i == len(s):
            return len(s)

        sub1 = self.longestSubstring(s[0:i], k)
        sub2 = self.longestSubstring(s[i + 1:], k)

        return max(sub1, sub2)
