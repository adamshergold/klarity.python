from typing import Optional

from leetcode.listnode import ListNode


class Solution:
    def swapPairs(self, head: Optional[ListNode]) -> Optional[ListNode]:

        prior, ptr = None, head

        while ptr is not None and ptr.next is not None:

            p_0, p_1, p_2 = ptr, ptr.next, ptr.next.next

            ptr.next = p_2
            p_1.next = p_0

            if prior is not None:
                prior.next = p_1
                prior = p_0
            else:
                head = p_1
                prior = p_0

            ptr = p_2

        return head
