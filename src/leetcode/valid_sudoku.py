from typing import List


class Solution:
    def isValidSudoku(self, board: List[List[str]]) -> bool:

        n_cols, n_rows = len(board[0]), len(board)

        # check the columns
        for col in range(n_cols):
            seen = set()
            for row in range(n_rows):
                v = board[row][col]
                if v != '.' and v in seen:
                    return False
                else:
                    seen.add(v)

        # check the rows
        for row in range(n_rows):
            seen = set()
            for col in range(n_cols):
                v = board[row][col]
                if v != '.' and v in seen:
                    return False
                else:
                    seen.add(v)

        def check_subbox(tl_row, tl_col):
            seen = set()
            for i in range(3):
                for j in range(3):
                    v = board[tl_row + i][tl_col + j]
                    if v != '.' and v in seen:
                        return False
                    else:
                        seen.add(v)
            return True

        # check the sub-boxes
        for row in range(3):
            for col in range(3):
                if not check_subbox(row * 3, col * 3):
                    return False

        return True
