class Solution:
    def convert(self, s: str, numRows: int) -> str:
        if numRows == 1:
            return s
        vs = [[] for _ in range(numRows)]
        dy, y = 1, 0
        for i in range(len(s)):
            vs[y].append(s[i])
            if y == 0 and dy < 0:
                dy = 1
            if dy > 0 and y == numRows - 1:
                dy = -1
            y += dy
        return ''.join([''.join(row) for row in vs])
