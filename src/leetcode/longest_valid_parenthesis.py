from collections import deque


class Solution:
    def longestValidParentheses(self, s: str) -> int:

        longest, stack = 0, deque()

        stack.appendleft(-1)

        for i in range(len(s)):
            if s[i] == '(':
                stack.appendleft(i)
            else:
                stack.popleft()
                if stack:
                    one_before_valid_starting_point = stack[0]
                    longest = max(longest, i - one_before_valid_starting_point)
                else:
                    stack.appendleft(i)

        return longest
