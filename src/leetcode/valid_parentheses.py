from collections import deque


class Solution:
    def isValid(self, s: str) -> bool:
        stack = deque()

        def is_open(c):
            return c in ('(', '{', '[')

        def match(o, c):
            if o == '(':
                return c == ')'
            if o == '{':
                return c == '}'
            if o == '[':
                return c == ']'
            return False

        for c in s:
            if is_open(c):
                stack.appendleft(c)
            else:
                if stack:
                    o = stack.popleft()
                    if not match(o, c):
                        return False
                else:
                    return False
        return not stack
