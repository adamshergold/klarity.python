class Solution:
    def findOnce(self, arr: list, n: int):
        if n == 1:
            return arr[0]
        i = 0
        while i < n:
            if i == n - 1:
                return arr[i]
            elif arr[i] != arr[i + 1]:
                return arr[i]
            else:
                i += 2
