class Solution():
    def maxSweetness(self, sweetness, n, k):

        lb = min(sweetness)
        ub = sum(sweetness)

        def slices(candidate):
            current, number_of_slices = 0, 0
            for i in range(len(sweetness)):
                current += sweetness[i]
                if current >= candidate:
                    number_of_slices += 1
                    current = 0
            return number_of_slices

        while lb+1 < ub:

            mid = (lb+ub)//2

            n_slices = slices(mid)

            if n_slices >= k+1:
                # can make too many so target needs to be 'larger'
                lb = mid
            else:
                # can't make enough so target needs to be 'smaller'
                ub = mid

        if slices(ub) == k+1:
            return ub

        return lb
