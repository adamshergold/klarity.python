class Solution:

    # Function to return a list of integers denoting spiral traversal of matrix.
    def spirallyTraverse(self, matrix, r, c):

        def impl(start_row, start_col, width, height):

            if width <= 0 or height <= 0:
                return []

            if width == 1:
                return [matrix[start_row+r][start_col] for r in range(0, height)]

            if height == 1:
                return matrix[start_row][start_col:start_col+width]

            impl_result = []

            for col in range(width):
                impl_result.append(matrix[start_row][start_col+col])

            for row in range(1, height-1):
                impl_result.append(matrix[start_row+row][start_col+width-1])

            for col in range(width):
                impl_result.append(matrix[start_row+height-1][start_col+width-col-1])

            for row in range(1, height-1):
                impl_result.append(matrix[start_row+height-row-1][start_col])

            return impl_result

        result, start_row, start_col = [], 0, 0

        number_of_iterations = (r+1)//2

        for i in range(number_of_iterations):
            result += impl(start_row, start_col, c-2*i, r-2*i)
            start_row += 1
            start_col += 1

        return result
