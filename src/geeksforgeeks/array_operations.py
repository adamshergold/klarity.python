from typing import List


class Solution:
    def arrayOperations(self, n: int, arr: List[int]) -> int:
        number_of_ops, flag = -1, False
        for i in range(len(arr)):
            if arr[i] == 0:
                number_of_ops = max(0, number_of_ops)
                if flag:
                    number_of_ops += 1
                    flag = False
            else:
                flag = True

        return number_of_ops + (1 if flag and number_of_ops >= 0 else 0)
