class Solution:
    def zigZag(self, arr, n):
        def swap(x, y):
            arr[x], arr[y] = arr[y], arr[x]

        for i in range(0, n-1):
            if i % 2 == 0:
                # going 'up' case
                if arr[i] > arr[i+1]:
                    swap(i, i+1)
            else:
                # going 'down' case
                if arr[i] < arr[i+1]:
                    swap(i, i+1)
