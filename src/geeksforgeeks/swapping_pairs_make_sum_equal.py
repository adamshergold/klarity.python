class Solution:
    def findSwapValues(self, a, n, b, m):

        a.sort()
        b.sort()

        # suppose s_a = sum(a) and s_b = sum(b)
        # then we are looking for two elements ea and eb such that
        # s_a - ea + eb = s_b - eb + ea
        # rearrange:
        # s_a - s_b = 2*( ea - eb )
        # or:
        # ea - eb = (s_a - s_b) /2
        # so we're looking for two elements that have a certain difference

        s_a, s_b = sum(a), sum(b)

        # no solution if difference is odd
        if (s_a - s_b) % 2 != 0:
            return -1

        target_diff = (s_a - s_b) // 2

        i, j = 0, 0

        while i < len(a) and j < len(b):
            if a[i] - b[j] == target_diff:
                return 1
            if a[i] - b[j] < target_diff:
                i += 1
            else:
                j += 1

        return -1
