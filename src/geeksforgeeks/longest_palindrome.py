class Solution:
    def longestPalin(self, S):

        if len(S) == 0:
            return ""

        def here(idx):
            l, r = idx, idx
            while l > 0 and S[l - 1] == S[idx]:
                l -= 1
            while r < len(S) - 1 and S[r + 1] == S[idx]:
                r += 1
            while l > 0 and r < len(S) - 1 and S[l - 1] == S[r + 1]:
                l -= 1
                r += 1
            return S[l:r + 1]

        longest_seen = ""

        for i in range(0, len(S)):
            longest_here = here(i)
            if len(longest_here) > len(longest_seen):
                longest_seen = longest_here

        return longest_seen
