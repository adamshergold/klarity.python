from typing import List


class Solution:
    def totalCuts(self, N: int, K: int, A: List[int]) -> int:
        right_mins = [0] * N
        right_mins[N - 1] = A[N - 1]
        for i in range(N - 2, -1, -1):
            right_mins[i] = min(A[i], right_mins[i + 1])
        count, l_max = 0, A[0]
        for i in range(1, N):
            if l_max + right_mins[i] >= K:
                count += 1
            l_max = max(l_max, A[i])
        return count
