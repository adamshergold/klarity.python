class Solution:
    def FirstNonRepeating(self, A):

        state = [0] * 26

        result = []

        for idx, c in enumerate(A):

            when = idx + 1

            index = ord(c) - ord('a')

            if state[index] == 0:
                state[index] = when
            elif state[index] > 0:
                state[index] = -state[index]

            char_to_add, min_when = None, None

            for i in range(26):
                this_when = state[i]
                if this_when > 0:
                    if min_when is None or this_when < min_when:
                        min_when = this_when
                        char_to_add = chr(ord('a') + i)

            result.append('#' if char_to_add is None else char_to_add)

        return ''.join(result)
