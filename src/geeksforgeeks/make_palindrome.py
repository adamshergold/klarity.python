from typing import List


class Solution:
    def makePalindrome(self, n: int, arr: List[str]) -> bool:
        count = {}
        for word in arr:
            count[word] = count.get(word, 0) + 1

        n_odd_self_palindromes = 0
        for word in count.keys():
            reversed_word = word[::-1]
            if word != reversed_word:
                word_count = count.get(word)
                reversed_word_count = count.get(reversed_word, 0)
                if word_count != reversed_word_count:
                    return False
            else:
                n_odd_self_palindromes += count.get(word) % 2

        return True if n_odd_self_palindromes <= 1 else False
