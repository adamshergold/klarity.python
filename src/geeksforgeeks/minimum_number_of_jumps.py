class Solution:

    def minJumps(self, arr, n):

        if n == 0:
            return -1

        if arr[0] == 0:
            return -1

        max_reachable_index, jumps, start, end = -1, 0, 0, 0

        # we maintain two pointers: 'start' and 'end' which are indices into
        # arr that tell us for the current start what is the range of jumpable
        # indices.  When we start these are both the first element

        while start < n:

            # from the current position i.e. 'start' what's the further index we can
            # move to next? i.e. arr[start] which contains number of jumps we can
            # go from here e.g. if arr = [3,2,1,1] and start = 1 then arr[1] == 2
            # so we could reach index 3 or 4 but 4 is the furthest!
            max_reachable_index = max(max_reachable_index, start + arr[start])

            # if we could get on or after the end then we are done by making one more jump
            if max_reachable_index >= n - 1:
                return jumps + 1

            # okay so they threw a spanner in the works with 0 being a valid entry in the array
            # if our max reachable index has value 0 then we walk backwards till we find a non-zero
            # entry as this wil be the furthest we should jump otherwise we're 'stuck'
            while arr[max_reachable_index] == 0:
                max_reachable_index -= 1

            # however, if we end up with the next point being the current start then we are stuck
            # e.g. consider [1,0] and start = 0 ... we can make one jump but it's to 0 so we can't
            # so our next jump ends up back at 0 i.e. next == start ... so there is no solution
            # which means return -1
            if max_reachable_index <= start:
                return -1

            # if start and end are the same then we make a jump and reset our start/end so that
            # the end moves out to the furthest reachable index
            if start == end:
                jumps += 1
                end = max_reachable_index

            # if start is still before the end we increment and look to see how far from this
            # point we could get, which might potentially update our furthest reachable point
            start += 1

        return -1
