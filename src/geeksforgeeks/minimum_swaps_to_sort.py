class Solution:

    def minSwaps(self, nums):

        positions, nums_sorted = {}, sorted(nums)

        for i in range(len(nums)):
            positions[nums[i]] = i

        def index_of(v):
            return positions.get(v)

        def swap(x, y):
            x_i, y_i = positions[nums[x]], positions[nums[y]]
            positions[nums[x]] = y_i
            positions[nums[y]] = x_i
            nums[x], nums[y] = nums[y], nums[x]

        number_of_swaps = 0

        # loop over each element (left to right)
        for i in range(len(nums)):
            # if at this position we don't have the expected
            # value then we need to do a swap
            if nums[i] != nums_sorted[i]:
                expected_number_is_at = index_of(nums_sorted[i])
                swap(expected_number_is_at, i)
                number_of_swaps += 1

        return number_of_swaps
