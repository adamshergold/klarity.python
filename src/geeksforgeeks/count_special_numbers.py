from collections import Counter


class Solution:
    def countSpecialNumbers(self, N, arr):
        counter = Counter(arr)
        set_arr = set(arr)
        is_special = set()
        max_arr = max(set_arr)
        for v in set_arr:
            candidate = v * 2
            while candidate <= max_arr:
                if candidate in set_arr:
                    is_special.add(candidate)
                candidate += v
        result = 0
        for v in set_arr:
            if v in is_special:
                result += counter[v]
            else:
                result += counter[v] if counter[v] > 1 else 0
        return result
