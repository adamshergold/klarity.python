from typing import List


class Solution:
    def minimumInteger(self, N: int, A: List[int]) -> int:

        # simple edge case i.e. single element satisfies the condition
        if N == 1:
            return A[0]

        # compute the sum
        s = sum(A)

        # now look for smallest meeting criteria
        smallest = None

        for i in range(0, N):
            # if this element meets criteria then we see if its smallest we've seen so far
            if s <= A[i] * N:
                smallest = min(A[i], smallest if smallest is not None else A[i])

        return smallest
