class Solution:
    def minSteps(self, s: str) -> int:
        count_of_consecutive_a, count_of_consecutive_b, i = 0, 0, 0
        while i < len(s):
            if s[i] == "a":
                count_of_consecutive_a += 1
                i += 1
                while i < len(s) and s[i] == "a":
                    i += 1
            else:
                count_of_consecutive_b += 1
                i += 1
                while i < len(s) and s[i] == "b":
                    i += 1
        return min(count_of_consecutive_b, count_of_consecutive_a) + 1
