from collections import deque


class Solution:

    def max_of_subarrays_0(self, arr, n, k):
        result = []
        for i in range(0, n-k+1):
            result.append(max(arr[i:i+k]))
        return result

    def max_of_subarrays(self, arr, n, k):
        dq = deque()
        result = []

        def add(index):
            while dq and arr[dq[-1]] <= arr[index]:
                dq.pop()
            dq.append(index)

        for i in range(k):
            add(i)

        for i in range(k, n):

            result.append(arr[dq[0]])

            while dq and dq[0] <= i - k:
                dq.popleft()

            add(i)

        result.append(arr[dq[0]])

        return result
