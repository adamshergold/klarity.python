class Solution:

    def minSwaps(self, nums):

        positions = {}

        nums_sorted = sorted(nums)

        for i in range(len(nums)):
            positions[nums[i]] = i

        n_swaps = 0
        for i in range(len(nums)):

            ev = nums_sorted[i]
            av = nums[i]

            if ev != av:
                # swap them ... first update the count
                n_swaps += 1

                # swap the values ...
                ev_currently_at = positions[ev]
                av_currently_at = i
                nums[ev_currently_at], nums[av_currently_at] = \
                    nums[av_currently_at], nums[ev_currently_at]

                # update the lookup table
                positions[av] = ev_currently_at
                positions[ev] = i

        return n_swaps
