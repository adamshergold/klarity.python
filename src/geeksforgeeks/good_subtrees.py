class Node:
    def __init__(self, val, left=None, right=None):
        self.data = val
        self.left = left
        self.right = right


class Solution:
    def goodSubtrees(self, root, k):
        good_count = 0

        def impl(node: Node):
            # this is not great having 'nonlocal' but simple/cheap
            nonlocal good_count
            result = {node.data}
            dl = impl(node.left) if node.left is not None else set()
            dr = impl(node.right) if node.right is not None else set()
            result = result.union(dl, dr)
            good_count += (1 if len(result) <= k else 0)
            return result

        impl(root)

        return good_count
