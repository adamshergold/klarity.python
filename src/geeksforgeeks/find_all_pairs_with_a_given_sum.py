
class Solution:
    def allPairs(self, A, B, N, M, X):
        set_b, result = set(B), []
        result = []
        for v in A:
            if X - v in set_b:
                result.append([v, X-v])

        return sorted(result, key=lambda x: x[0])
