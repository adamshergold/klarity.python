class Solution:
    def firstElementKTime(self, a, n, k):
        element_count = {}
        for v in a:
            element_count[v] = element_count.get(v, 0) + 1
            if element_count[v] >= k:
                return v
        return -1
