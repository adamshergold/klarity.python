class Solution:
    def maxSubArraySum(self, arr, N):
        if len(arr) == 0:
            return -1
        # the idea with kadane ... suppose for a given index i
        # we know two things: the max subarray sum in arr[0..i-1] and
        # we know the max subarray sum ending at i-1 ... let's call
        # these max_sum and max_ending_here. what about when we
        # consider arr[0..i]? well, the max_ending_here is the
        # largest of max_ending_here + arr[i] and arr[i] and
        # if this is larger than the current max then it is the new max
        max_sum = arr[0]
        max_ending_here = arr[0]
        for i in range(1, len(arr)):
            max_ending_here = max(max_ending_here + arr[i], arr[i])
            max_sum = max(max_ending_here, max_sum)
        return max_sum
