class Solution:
    def equalSum(self, A, N):

        if N == 0:
            return -1

        if N == 1:
            return 1

        l_sum, r_sum = A[0], sum(A[2:])

        for i in range(1, N-1):
            if l_sum == r_sum:
                return i+1
            l_sum += A[i]
            r_sum -= A[i+1]

        return -1
