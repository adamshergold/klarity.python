"""Solution module"""


class Solution:
    def isSubsetSum(self, N, arr, sum):
        """Solution implementation"""
        if len(arr) == 0:
            return sum == 0

        # setup dynamic programming state.
        # dp[i][j] is the number of subsets summing to 'j' for vs[0..i] (inclusive)
        # note special case that there's always a subset adding to 0 i.e. the empty set
        dp = [[True if j == 0 else False for j in range(0, sum+1)] for _ in range(0, len(arr))]

        # the loop goes through vs from start to end
        for i in range(0, len(arr)):
            for j in range(0, sum+1):
                if arr[i] > j:
                    # if current element is bigger than sum then it can't be part
                    # of a set summing to 'j' so use previous count
                    dp[i][j] = dp[i-1][j]
                else:
                    dp[i][j] = dp[i-1][j] or dp[i-1][j-arr[i]]

        return dp[len(arr)-1][sum]
