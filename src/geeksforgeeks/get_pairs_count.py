from collections import Counter


class Solution:
    def getPairsCount(self, arr, n, k):
        # early return if 0 or 1 element i.e. no pairs!
        if len(arr) < 2:
            return 0
        # create a map of distinct values in arr and their count
        # we use the handy collections module to help
        frequency_counter = Counter(arr)
        number_of_pairs = 0
        for v in arr:
            number_of_pairs += frequency_counter.get(k-v, 0)

            if v == k-v:
                number_of_pairs -= 1

        return number_of_pairs // 2
