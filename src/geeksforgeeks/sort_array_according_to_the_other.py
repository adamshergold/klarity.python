from collections import Counter


class Solution:
    # A1[] : the input array-1
    # N : size of the array A1[]
    # A2[] : the input array-2
    # M : size of the array A2[]

    # Function to sort an array according to the other array.
    def relativeSort(self, A1, N, A2, M):
        A1.sort()
        count = Counter(A1)
        a2_set = set(A2)
        result = []
        for v in A2:
            occurences = count.get(v, 0)
            for _ in range(occurences):
                result.append(v)
        for v in A1:
            if v in a2_set:
                continue
            result.append(v)
        return result
