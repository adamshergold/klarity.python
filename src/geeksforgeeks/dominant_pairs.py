from typing import List


class Solution:
    def dominantPairs(self, n: int, arr: List[int]) -> int:
        n_over_2 = n // 2
        lhs = sorted(arr[0:n_over_2], reverse=True)
        rhs = [5 * v for v in sorted(arr[n_over_2:], reverse=True)]
        i, j = 0, 0
        n_pairs = 0
        while i < n_over_2 and j < n_over_2:
            while j < n_over_2 and rhs[j] > lhs[i]:
                j += 1
            n_pairs += n_over_2 - j
            i += 1
        return n_pairs
