class Solution:
    def minimumSum(self, s: str) -> int:

        def mirror(i):
            s[len(s) - 1 - i]

        chars = [c for c in s]
        i, j = 0, len(chars) - 1

        while i < j:
            if chars[i] != chars[j]:
                if chars[i] == '?':
                    chars[i] = chars[j]
                elif s[j] == '?':
                    chars[j] = chars[i]
                else:
                    return -1
            i += 1
            j -= 1

        n_over_2 = len(chars) // 2

        limit = n_over_2 - 1 if len(chars) % 2 == 0 else n_over_2

        i = 0
        while i <= limit and chars[i] == '?':
            i += 1

        prev = chars[i] if chars[i] != '?' else 'a'

        for i in range(0, limit + 1):
            if chars[i] == '?':
                chars[i] = prev
                chars[len(chars) - 1 - i] = chars[i]
            prev = chars[i]

        total = 0
        for i in range(1, len(chars)):
            total += abs(ord(chars[i]) - ord(chars[i - 1]))
        return total
