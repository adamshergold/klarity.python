class Solution:
    def check(self, A, B, N):
        values = {}
        for v in A:
            values[v] = values.get(v, 0) + 1
        for v in B:
            values[v] = values.get(v, 0) - 1
        for k in values:
            if values[k] != 0:
                return False
        return True
