class Solution:
    def peakElement(self, arr, n):
        if len(arr) == 1:
            return 0
        for i in range(0, len(arr)):
            left_smaller = True if i == 0 else arr[i-1] <= arr[i]
            right_smaller = True if i == n-1 else arr[i+1] <= arr[i]
            if left_smaller and right_smaller:
                return i
        return -1
