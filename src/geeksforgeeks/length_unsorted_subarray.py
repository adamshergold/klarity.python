class Solution:

    def printUnsorted(self, arr, n):
        start, end = 0, n - 1
        while start < n - 1 and arr[start + 1] >= arr[start]:
            start += 1
        while end > 1 and arr[end - 1] <= arr[end]:
            end -= 1
        min_v, max_v = arr[start], arr[start]
        for i in range(start, end + 1):
            min_v = min(min_v, arr[i])
            max_v = max(max_v, arr[i])

        i = 0
        while i < start and arr[i] <= min_v:
            i += 1
        start = i

        i = n - 1
        while i > end and arr[i] >= max_v:
            i -= 1
        end = i

        return [start, end]
