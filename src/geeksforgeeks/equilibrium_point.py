class Solution:
    def equilibriumPoint(self, A, N):

        # take care of edge cases
        # empty or 2 element array cannot have equilibrium point
        if N == 0 or N == 2:
            return -1

        # single element array means the only element is equilibrium
        # by construction
        if N == 1:
            return 1

        # we loop, using i, over A from index [2..N-1] and as we go we maintain
        # two sums: the sum of [0..i-1] and sum [i+1..N-1].  if these become equal
        # then we have found an equilibrium point and we return it

        # initialise the state
        left_sum, right_sum = A[0], sum(A[2:])

        # start looping
        for i in range(1, N-1):
            # have we found it? if so return (remember result in 1-based hence the +1)
            if left_sum == right_sum:
                return i+1

            # otherwise we move along and adjust the left and right sums
            left_sum += A[i]
            right_sum -= A[i+1]

        # if we get here then we've not got a equilibrium either
        return -1
