class Solution:
    def minValue(self, a, b, n):
        a.sort()
        b.sort(reverse=True)
        total = 0
        for (x, y) in zip(a, b):
            total += x*y
        return total
