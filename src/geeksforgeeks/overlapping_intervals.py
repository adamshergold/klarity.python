from functools import cmp_to_key


class Solution:
    def overlappedInterval(self, Intervals):

        def cmp(x, y):
            return x[0] - y[0]

        sorted_intervals = sorted(Intervals, key=cmp_to_key(cmp))

        index = 0

        for i in range(1, len(sorted_intervals)):
            # the question is: do we merge interval i into where index is pointing?
            # we do if interval at index ends on/after the start of i
            if sorted_intervals[index][1] >= sorted_intervals[i][0]:
                sorted_intervals[index][1] = max(sorted_intervals[index][1], sorted_intervals[i][1])
            else:
                index += 1
                sorted_intervals[index] = sorted_intervals[i]

        # return the slice up to index (which is last valid interval)
        return sorted_intervals[0:index + 1]
