class Solution:
    def kthElement_0(self, arr1, arr2, n, m, k):
        count, i, j = 0, 0, 0
        while count < k:
            while i < len(arr1) and arr1[i] < arr2[j]:
                i += 1
                count += 1
                if count == k:
                    return arr1[i-1]
            while j < len(arr2) and arr2[j] < arr1[i]:
                j += 1
                count += 1
                if count == k:
                    return arr2[j-1]
        return -1
