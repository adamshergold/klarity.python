class Solution:
    def sort012(self, arr, n):
        # since we know the array only has 0, 1 and 2 then lets just count how many of each
        n_0, n_1, n_2 = 0, 0, 0
        for v in arr:
            if v == 0:
                n_0 += 1
            if v == 1:
                n_1 += 1
            if v == 2:
                n_2 += 1
        # now we have the count it's easy:
        arr[0:n_0] = [0]*n_0
        arr[n_0:n_0+n_1] = [1]*n_1
        arr[n_0+n_1:] = [2]*n_2
