from typing import List


class Solution:
    def makeChanges(self, N: int, K: int, target: int, coins: List[int]) -> bool:
        # dynamic programming again
        # dp[i][j] = can we make i using j coins
        # so i will range from [0..target] and j from [0..K]
        dp = [[False] * (K + 1) for i in range(target + 1)]
        # special case: can always make 0 with 0 coins
        dp[0][0] = True

        for i in range(1, target + 1):
            for j in range(K + 1):
                for coin in coins:
                    # if this coin is smaller than the target (okay to consider)
                    # and if we can make 'i-coin' using 1 fewer coin (j-1) then
                    # we are good i.e. we can make i using this coin with a total
                    # of j coins used
                    if coin <= i and j > 0 and dp[i - coin][j - 1]:
                        dp[i][j] = True
                        break

        return dp[target][K]
