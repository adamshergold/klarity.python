class Solution:
    def minimumPlatform(self, n, arr, dep):

        if len(arr) != len(dep):
            return -1

        if len(arr) == 0:
            return 0

        if len(arr) == 1:
            return 1

        arr.sort()
        dep.sort()

        max_count, count, i, j = 0, 0, 0, 0

        while i < len(arr) and j < len(dep):
            if arr[i] <= dep[j]:
                count += 1
                i += 1
            else:
                count -= 1
                j += 1

            max_count = max(max_count, count)

        return max_count
