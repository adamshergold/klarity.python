import functools


class Solution:

    def printLargest(self, arr):

        def order(a, b):
            ab = int(a+b)
            ba = int(b+a)
            return ba - ab

        xs = map(str, arr)
        ys = sorted(xs, key=functools.cmp_to_key(order))

        return ''.join(ys)
