class Solution:
    def longestCommonSubstring(self, X, Y):

        # define LCS[i][j] = length of longest common prefix of X[0..i-1] and Y[0..j-1]
        # we can define this recursively as
        # LCS[i][j] = 0 if either i or j = 0 (base case)
        # LCS[i][j] = LCS[i-1][j-1] + 1 if X[i-1] == Y[j-1]

        lcs = [[0]*(len(Y)+1) for _ in range(len(X)+1)]
        max_lcs_seen = 0
        for i in range(len(X)+1):
            for j in range(len(Y)+1):
                if i == 0 or j == 0:
                    lcs[i][j] = 0
                else:
                    lcs[i][j] = lcs[i-1][j-1] + 1 if X[i-1] == Y[j-1] else 0
                max_lcs_seen = max(max_lcs_seen, lcs[i][j])
        return max_lcs_seen
