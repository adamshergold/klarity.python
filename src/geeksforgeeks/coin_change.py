class Solution:
    def count_0(self, coins, N, Sum):

        if Sum == 0:
            return 1

        if Sum < 0:
            return 0

        if N <= 0:
            return 0

        return self.count(coins, N-1, Sum) + self.count(coins, N, Sum - coins[N-1])

    def count(self, coins, N, Sum):

        if Sum == 0:
            return 1

        if Sum < 0:
            return 0

        if N <= 0:
            return 0

        dp = [[1 if i == 0 else 0]*N for i in range(0, Sum+1)]

        for i in range(1, Sum+1):
            for j in range(0, N):
                this_coin = coins[j]
                without_using_this_coin = dp[i][j-1] if j >= 1 else 0
                with_this_coin = dp[i-this_coin][j] if i >= this_coin else 0
                dp[i][j] = with_this_coin + without_using_this_coin

        return dp[Sum][N-1]
