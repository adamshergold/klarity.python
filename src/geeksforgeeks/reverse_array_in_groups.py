class Solution:
    def reverseInGroups(self, arr, N, K):
        if N == 0:
            return

        def reverse(start, end):
            while start < end:
                arr[start], arr[end] = arr[end], arr[start]
                start += 1
                end -= 1

        i = 0
        while i < N:
            reverse(i, min(i+K, N)-1)
            i += K
