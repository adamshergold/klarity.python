
class Solution:
    def longestCommonPrefix(self, arr, n):

        if len(arr) == 0:
            return "-1"

        i = 0
        while True:
            for j in range(0, n):
                if len(arr[j]) == i or arr[j][i] != arr[0][i]:
                    return "-1" if i == 0 else arr[0][0:i]
            i += 1
