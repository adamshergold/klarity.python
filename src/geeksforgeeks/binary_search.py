class Solution:
    def binarysearch(self, arr, n, k):
        if n == 0:
            return -1
        i, j = 0, n-1
        while i != j:
            mid = (i + j) // 2

            if arr[mid] == k:
                return mid

            if arr[mid] < k:
                i = mid + 1
            else:
                j = mid

        return i if arr[i] == k else -1
