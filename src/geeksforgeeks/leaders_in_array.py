class Solution:
    def leaders(self, A, N):
        if N == 0:
            return []

        max_to_the_right, current_max = [0]*N, -1

        for i in range(N-1, -1, -1):
            current_max = max(A[i], current_max)
            max_to_the_right[i] = current_max

        result = []

        for i in range(0, N):
            if A[i] >= max_to_the_right[i]:
                result.append(A[i])

        return result
