class Solution:
    def stringMirror(self, s: str) -> str:
        i = 1
        while i < len(s) and s[i] < s[i-1]:
            i += 1
        s1 = s[0:i]
        s1 += s1[::-1]
        i = 1
        while i < len(s) and s[i] <= s[i-1]:
            i += 1
        s2 = s[0:i]
        s2 += s2[::-1]
        return s1 if s1 < s2 else s2
