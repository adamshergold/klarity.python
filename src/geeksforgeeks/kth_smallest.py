class Solution:
    def kthSmallest(self, arr, left, right, k):
        '''
        arr : given array
        l : starting index of the array i.e 0
        r : ending index of the array i.e size-1
        k : find kth smallest element and return using this function
        '''

        def swap(x, y):
            arr[x], arr[y] = arr[y], arr[x]

        def partition(start, end):
            m = start-1
            pivot = arr[end]
            for i in range(start, end):
                if arr[i] < pivot:
                    m += 1
                    swap(m, i)
            swap(m+1, end)
            return m+1

        def impl(start, end, kth):

            p = partition(start, end)

            # edge case ... we are pointing at minimum
            if p == start:

                # if we want the 1st smallest we are done
                if kth == 1:
                    return arr[p]

                # otherwise we look for the (k-1)th smallest
                return impl(start+1, end, kth-1)

            # how many elements are less than the pivot?
            number_of_elements_less_than_pivot = p - start

            # if there are target_k - 1 then pivot must be kth smallest ...
            if number_of_elements_less_than_pivot == kth - 1:
                return arr[p]

            # if there are more elements then we keep searching for kth smallest in the smaller pool
            if number_of_elements_less_than_pivot > (kth - 1):
                return impl(start, p-1, kth)

            # otherwise we've reduced the problem to looking in the set of larger elements
            # but no longer looking for the kth smallest but 'kth - number less than pivot'
            return impl(p, end, kth - number_of_elements_less_than_pivot)

        if right < left:
            return -1

        return impl(left, right, k)
