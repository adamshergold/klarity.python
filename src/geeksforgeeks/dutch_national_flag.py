class Solution:
    def sort012(self, arr, n):
        l, m, h = 0, 0, n - 1

        # the invariant is
        # arr[0..l-1] are 0
        # arr[l..m-1] are 1
        # arr[h+1..n-1] are 2
        # arr[m..h] are unknown

        def swap(x, y):
            arr[x], arr[y] = arr[y], arr[x]

        # keep looping while m <= h i.e. there are 'unknown'
        while m <= h:

            # depending on the value at mid we swap to move it
            # into the right place

            if arr[m] == 0:
                swap(l, m)
                l += 1
                m += 1
            elif arr[m] == 1:
                m += 1
            else:
                # case arr[m] == 2
                swap(h, m)
                h -= 1

        return
