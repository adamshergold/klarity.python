class Solution:
    def duplicates(self, arr, n):
        # values in arr are in range [0..n-1]

        # create result array
        result = []

        # so what's the plan? the idea is that we'll go through the array by index 'i'
        # we look at the value at index arr[i] and if it's positive we make it negative ...
        # .. if its already negative then we've seen arr[i] before so we can add to result
        # and then we scale the value by -(n+1) to make it positive again but outside
        # the range so we know we can skip this value
        # there is one quirk which is that 0 and -0 are not distinct ... so we avoid
        # this problem by incrementing all our values by 1 before we start
        # (so we must remember to -1 appropriately)

        # arr modified to be range [1..n]
        for i in range(0, n):
            arr[i] += 1

        def recover(at):
            if arr[at] < 0:
                return -arr[at]
            if arr[at] > n:
                return arr[at] // (n+1)
            return arr[at]

        for i in range(0, n):

            # first recover the original value that was here (but it's still '+1' so not
            # really the original one ... but the modified original :-)

            original_value_at_i = recover(i)

            index_for_original_value = original_value_at_i - 1

            if arr[index_for_original_value] < 0:
                result.append(original_value_at_i-1)
                arr[index_for_original_value] *= -(n+1)
            elif arr[index_for_original_value] > n:
                continue
            else:
                arr[index_for_original_value] *= -1

        return sorted(result) if len(result) > 0 else [-1]
