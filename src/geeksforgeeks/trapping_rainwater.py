class Solution:
    def trappingWater(self, arr, n):

        def calculate_left_maxes():
            result = []
            current_max = 0
            for i in range(0, n):
                current_max = max(arr[i], current_max)
                result.append(current_max)
            return result

        def calculate_right_maxes():
            result = []
            current_max = 0
            for i in range(n-1, -1, -1):
                current_max = max(arr[i], current_max)
                result.append(current_max)
            result.reverse()
            return result

        total = 0

        left_maxes = calculate_left_maxes()
        right_maxes = calculate_right_maxes()

        for i in range(0, n):
            max_left = left_maxes[i]
            max_right = right_maxes[i]
            total += max(0, min(max_left, max_right) - arr[i])

        return total
