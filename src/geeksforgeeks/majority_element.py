class Solution:
    def majorityElement(self, A, N):

        # deal with a few edge cases
        # empty array has no majority
        if N == 0:
            return -1

        # single element has trivial majority i.e. the only element
        if N == 1:
            return A[0]

        # we loop over the array keeping track of a 'candidate' majority element
        # along with a counter. To start we set these to the first
        # element with a counter of 1
        candidate, counter = A[0], 1

        # now loop over the remaining elements
        for i in range(1, N):

            # if the current element is same as candidate then bump the count
            if A[i] == candidate:
                counter += 1
            # if not, we reduce the counter
            else:
                counter -= 1

            # if the counter is zero then we have a new candidate i.e. the current value
            if counter == 0:
                candidate = A[i]
                counter = 1

        # now we go and count the number of elements that are the candidate value
        actual_count = sum([1 for v in A if v == candidate])

        # if the actual count is > n/2 then yes, it's a real majority ... otherwise not
        if actual_count > N//2:
            return candidate

        return -1
