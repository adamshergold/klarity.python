class Solution:

    # Function to return max value that can be put in knapsack of capacity W.
    def knapSack_recursive_memoised(self, W, wt, val, n):
        memo = {}

        def impl(capacity, i):

            memo_key = str(capacity) + ':' + str(i)

            if memo.get(memo_key) is not None:
                return memo.get(memo_key)

            if i == 0:
                result = val[0] if wt[0] <= capacity else 0
            else:
                max_with = impl(capacity - wt[i], i - 1) + val[i] if wt[i] <= capacity else 0
                max_without = impl(capacity, i - 1)

                result = max(max_without, max_with)

            memo[memo_key] = result

            return result

        return impl(W, n - 1)

    def knapSack_dp(self, W, wt, val, n):
        dp = [[0] * n for _ in range(0, W + 1)]

        for i in range(W + 1):
            for j in range(n):
                if j == 0:
                    with_item_j = val[0] if wt[0] <= i else 0
                else:
                    with_item_j = dp[i - wt[j]][j - 1] + val[j] if wt[j] <= i else 0

                without_j = dp[i][j - 1] if j > 0 else 0

                dp[i][j] = max(with_item_j, without_j)
        return dp[W][n - 1]
