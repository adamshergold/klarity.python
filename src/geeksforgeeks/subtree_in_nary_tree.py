# User function Template for python3

class Node:
    def __init__(self, key, children=None):
        self.key = key
        self.children = children or []

    def __str__(self):
        return str(self.key)


class Solution:

    def createRoot(self, s: str):

        i, items, current = 0, [], []

        while i < len(s):
            # skip white space
            while i < len(s) and s[i] == ' ':
                i += 1
            if s[i] == 'N':
                items.append(current)
                current = []
                i += 1
            else:
                j = i
                while j < len(s) and s[j].isdigit():
                    j += 1
                current.append(int(s[i:j]))
                i = j + 1

        items.append(current)

        if len(items) == 0:
            return None

        root = Node(items[0][0])

        i, q, = 1, [root]

        while i < len(items):
            next_nodes, n_children = [], len(q)
            for j in range(n_children):
                nodes = list(map(lambda v: Node(v), items[i + j]))
                q[j].children = nodes
                next_nodes += nodes
            i += n_children
            q = next_nodes

        return root

    def duplicateSubtreeNaryTree(self, root):

        paths = {}

        def dfs(ptr):
            suffix = '' if (ptr.children is None or len(ptr.children) == 0) \
                else ''.join(list(map(lambda n: dfs(n), ptr.children)))
            nid = ':'+str(ptr.key)+suffix
            paths[nid] = paths.get(nid, 0) + 1
            return nid

        dfs(root)

        count = 0

        for (k, v) in paths.items():
            count += 1 if v > 1 else 0

        return count
