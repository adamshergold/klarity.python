
class Solution:
    def findSubArrays_0(self, arr, n):
        result = []
        for start in range(0, n):
            for end in range(start+1, n+1):
                subarray = arr[start:end]
                if sum(subarray) == 0:
                    result.append(subarray)
        return len(result)

    def findSubArrays(self, arr, n):
        sums, sum_to_i, result = {}, 0, 0
        for i in range(n):

            sum_to_i += arr[i]

            if sum_to_i == 0:
                result += 1

            result += sums.get(sum_to_i, 0)

            sums[sum_to_i] = sums.get(sum_to_i, 0) + 1

        return result
