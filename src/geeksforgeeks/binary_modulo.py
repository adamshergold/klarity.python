class Solution():
    def modulo(self, s, m):
        v = 0
        for i in range(len(s)):
            v = (v * 2 + (1 if s[i] == '1' else 0)) % m
        return v % m
