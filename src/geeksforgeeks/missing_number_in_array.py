class Solution:
    def MissingNumber(self, arr, n):
        # Array of numbers [1..n ] should have sum n(n+1)/2
        # so we rely on this simple formula.  How?
        # we know our array should sum to n(n+1)/2 so we
        # compare this value to the actual sum ... the difference
        # is the missing number!
        actual_sum = sum(arr)
        expected_sum = n*(n+1) // 2
        return expected_sum - actual_sum
