class Solution:

    def getMinDiff(self, arr, n, k):

        # I do not understand this algorithm ... need to think about it more but it
        # 'works' yet I can't find a good explanation of why.  Most unsatisfactory.
        arr.sort()

        result = arr[n-1] - arr[0]

        for i in range(1, n):

            max_h = max(arr[n-1] - k, arr[i-1] + k)

            min_h = min(arr[0] + k, arr[i] - k)

            if min_h < 0:
                continue

            result = min(result, max_h - min_h)

        return result
