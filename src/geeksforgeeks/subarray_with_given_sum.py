class Solution:
    def subArraySum(self, arr, n, s):
        # early return for simple case of empty array
        if len(arr) == 0:
            return [-1]
        # idea is two pointers: 'start' and 'end'
        # if sum(arr[start..end]) == s we are done
        # if it's > s then we remove element at start and move start along one
        # if it's < s then we extend the right
        start = 0
        end = 0
        start_end_sum = arr[0]
        while start < len(arr) and end < len(arr):

            if start_end_sum == s:
                return [start+1, end+1]

            if start_end_sum < s:
                # if running sum is still less we want to advance
                # the end and add the value there ... unless we go off the end
                end += 1
                if end == len(arr):
                    return [-1]
                start_end_sum += arr[end]

            if start_end_sum > s:
                # if running sum is now bigger then two choices:
                #   if start == end then we move both along
                #     (but check we don't go off the end!)
                #     else we move the start (and decrement the total)
                if start == end:
                    start += 1
                    end += 1
                    if start == len(arr):
                        return [-1]
                    start_end_sum = arr[start]
                else:
                    start_end_sum -= arr[start]
                    start += 1
