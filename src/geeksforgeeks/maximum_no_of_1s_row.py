class Solution:
    def maxOnes(self, Mat, N, M):

        def number_of_ones(r):

            if Mat[r][M-1] == 0:
                return 0

            if Mat[r][0] == 1:
                return M

            i, j = 0, M
            while i+1 < j:
                mid = (i+j) // 2
                if Mat[r][mid] == 0:
                    i = mid
                else:
                    j = mid

            return M-j

        max_i, max_len = 0, 0

        for r in range(N):
            number_of_ones_in_r = number_of_ones(r)
            if number_of_ones_in_r > max_len:
                max_len = number_of_ones_in_r
                max_i = r

        return max_i
