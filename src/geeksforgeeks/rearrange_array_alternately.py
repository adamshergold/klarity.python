class Solution:
    def rearrange(self, arr, n):

        # we'll make use of the following 'trick' to store two values in one.
        # if we know the values are in a certain range e.g. [0..N] then we can store
        # two values x and y (that are in this range) by using % and division.
        # first set M = N + 1 i.e. one larger than the largest value then we can store x and y
        # as V = x * M + y.  Why does this work?  Well, if you have V then you can
        # back-out x and y as:
        # x = V // M
        # y = V % M
        # neat huh?

        # our approach will be to pass through the array twice: first time we'll store two values
        # in each place: the original value and the desired value (using this trick)
        # and then we pass through again to pull out the desired values

        M = max(arr) + 1

        max_index, min_index = n-1, 0

        for i in range(n):
            if i % 2 == 0:
                # in even indices we want to store the descending max values
                value_to_store_raw = arr[max_index]
                # however, we have to be careful: arr[max_index] might already have
                # been scaled up so we need to extract the original value there
                # by using the modulus
                value_to_store = value_to_store_raw % M
                # we could simply this expression but i've left it like this
                # as (I think) its clear to see what's going on
                arr[i] = value_to_store * M + arr[i]
                max_index -= 1
            else:
                value_to_store_raw = arr[min_index]
                value_to_store = value_to_store_raw % M
                arr[i] = value_to_store * M + arr[i]
                min_index += 1

        for i in range(n):
            arr[i] = arr[i] // M
