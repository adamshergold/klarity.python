def isSubset(a1, a2, n, m):
    count_of_values_in_a1 = {}
    for v in a1:
        count_of_values_in_a1[v] = count_of_values_in_a1.get(v, 0) + 1
    for v in a2:
        count_of_values_in_a1[v] = count_of_values_in_a1.get(v, 0) - 1
        if count_of_values_in_a1[v] < 0:
            return "No"
    return "Yes"
