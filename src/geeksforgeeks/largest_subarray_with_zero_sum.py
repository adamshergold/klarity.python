class Solution:
    def maxLen(self, n, arr):
        earliest_index_that_sums_to_key_value = {}
        running_total = 0
        max_length = 0
        for i in range(0, n):
            running_total += arr[i]
            if running_total == 0:
                max_length = i+1
            if earliest_index_that_sums_to_key_value.get(running_total) is not None:
                earlier_index = earliest_index_that_sums_to_key_value.get(running_total)
                max_length = max(max_length, i - earlier_index)
            else:
                earliest_index_that_sums_to_key_value[running_total] = i
        return max_length
