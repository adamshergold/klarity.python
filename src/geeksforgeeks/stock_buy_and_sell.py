class Solution:
    def stockBuySell(self, A, n):
        i, result = 0, []
        while i < n-1:
            # keep moving forward till find a price going up in the next
            # step ... this is a 'buy' opportunity
            while i < n-1 and A[i+1] <= A[i]:
                i += 1
            buy = i
            # now keep moving until we find a drop in the next step
            # ... this is when we should sell
            while i < n-1 and A[i+1] >= A[i]:
                i += 1
            if buy != i:
                result.append([buy, i])
        return result
